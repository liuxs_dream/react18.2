// 每个虚拟节点都对应一个类型的
export const FunctionComponent = 0; // 函数组件
export const ClassComponent = 1; // 类组件
export const IndeterminateComponent = 2; // 未知的
export const HostRoot = 3; // 根节点
export const HostComponent = 5; // 原生的dom节点
export const HostText = 6; // 文本节点
export const SuspenseComponent = 13;
export const ContextConsumer = 9;
export const ContextProvider = 10;
