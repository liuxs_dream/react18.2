import {
  NoLanes,
  NoTimestamp,
  SyncLane,
  markRootUpdated,
  includesSyncLane,
  getNextLanes,
  NoLane,
  getHighestPriorityLane,
  includesBlockingLane,
  includesExpiredLane,
  markStarvedLanesAsExpired,
  markRootFinished,
  mergeLanes,
} from "./ReactFiberLane";
import {
  NoFlags,
  BeforeMutationMask,
  MutationMask,
  LayoutMask,
  PassiveMask,
  Passive,
} from "./ReactFiberFlags";
import {
  commitPassiveUnmountEffects,
  commitPassiveMountEffects,
  commitLayoutEffects,
} from "./ReactFiberCommitWork";
import { ConcurrentMode, NoMode } from "./ReactTypeOfMode";
import {
  ImmediatePriority as ImmediateSchedulerPriority,
  UserBlockingPriority as UserBlockingSchedulerPriority,
  NormalPriority as NormalSchedulerPriority,
  IdlePriority as IdleSchedulerPriority,
  now,
  shouldYield,
  scheduleCallback as Scheduler_scheduleCallback,
  cancelCallback as Scheduler_cancelCallback,
} from "./Scheduler";
import { deferRenderPhaseUpdateToNextBatch } from "../shared/ReactFeatureFlags";
import { LegacyRoot } from "./ReactRootTags";
import { finishQueueingConcurrentUpdates } from "./ReactFiberConcurrentUpdates";
import {
  scheduleLegacySyncCallback,
  scheduleSyncCallback,
  flushSyncCallbacks,
} from "./ReactFiberSyncTaskQueue";
import { supportsMicrotasks, scheduleMicrotask } from "./ReactFiberHostConfig";
import { getCurrentEventPriority } from "../react-dom-bindings/client/ReactDOMHostConfig";
import {
  DiscreteEventPriority,
  ContinuousEventPriority,
  DefaultEventPriority,
  IdleEventPriority,
  getCurrentUpdatePriority,
  setCurrentUpdatePriority,
  lowerEventPriority,
  lanesToEventPriority,
} from "./ReactEventPriorities";
import { createWorkInProgress } from "./ReactFiber";
import { disableSchedulerTimeoutInWorkLoop } from "../shared/ReactFeatureFlags";

import { commitMutationEffects } from "./ReactFiberCommitWork.js";
import { completeWork } from "./ReactFiberCompleteWork";
import { beginWork as originalBeginWork } from "./ReactFiberBeginWork";
let beginWork = originalBeginWork;
let scheduleCallback = Scheduler_scheduleCallback;

export const NoContext = 0b000;
export const BatchedContext = 0b001; // 批量的
export const RenderContext = 0b010;
export const CommitContext = 0b100;

const RootInProgress = 0; // 构建fiber树进行进行中
const RootFatalErrored = 1;
const RootErrored = 2;
const RootSuspended = 3;
const RootSuspendedWithDelay = 4;
const RootCompleted = 5; // 完成
const RootDidNotComplete = 6;

let executionContext = NoContext;
let workInProgressRootRenderLanes = NoLanes;

let workInProgressRoot = null; // 当前工作的根
let workInProgress = null;
// 渲染工作结束的时候 当前的fiber树处于的状态 默认是进行中的
// completeUnitOfWork的时候进行修改
let workInProgressRootExitStatus = RootInProgress;

// 有 useEffect副作用的根节点
let rootWithPendingPassiveEffects = null;
// 此根节点上是否有 Effect类型的副作用
let rootDoesHavePassiveEffects = false;

export let renderLanes = NoLanes;

export function flushSync(fn) {
  const prevExecutionContext = executionContext;
  // 执行的上下文 批量模型
  executionContext |= BatchedContext;
  // 1. 获取优先级
  // const previousPriority = 0; // NoLane 先写死
  const previousPriority = getCurrentUpdatePriority();
  // 2. 执行fn
  try {
    // 设置优先级
    setCurrentUpdatePriority(DiscreteEventPriority);
    if (fn) return fn();
    else return undefined;
  } finally {
    // 还原上次的优先级
    setCurrentUpdatePriority(previousPriority);
    executionContext = prevExecutionContext;
    // 刷新 immediate 的回调 初次渲染执行
    if ((executionContext & (RenderContext | CommitContext)) === NoContext) {
      // console.log(
      //   "初次渲染 flushSync(updateContainer)的时候执行finally会到这个地方清空"
      // );
      flushSyncCallbacks();
    }
  }
}

// 当前事件的时间
let currentEventTime = NoTimestamp; // -1

export function requestEventTime() {
  if ((executionContext & (RenderContext | CommitContext)) !== NoContext) {
    // We're inside React, so it's fine to read the actual time.
    return now();
  }
  if (currentEventTime !== NoTimestamp) return currentEventTime;
  // performance.now() || Date.now()
  currentEventTime = now();
  return currentEventTime;
}

// 获取赛道lane
export function requestUpdateLane(fiber) {
  const mode = fiber.mode; // fiber的mode
  if ((mode & ConcurrentMode) === NoMode) {
    // 同步赛道
    return SyncLane;
  } else if (
    (executionContext & RenderContext) !== NoContext &&
    workInProgressRootRenderLanes !== NoLanes
  ) {
    // return pickArbitraryLane(workInProgressRootRenderLanes);
  }
  // 获取当前的更新优先级
  const updateLane = getCurrentUpdatePriority();
  if (updateLane !== NoLane) {
    return updateLane;
  }
  // 获取当前事件的优先级 DefaultEventPriority
  const eventLane = getCurrentEventPriority();
  return eventLane;
}

export function isUnsafeClassRenderPhaseUpdate(fiber) {
  return (
    (!deferRenderPhaseUpdateToNextBatch ||
      (fiber.mode & ConcurrentMode) === NoMode) &&
    (executionContext & RenderContext) !== NoContext
  );
}

/**
 * 从根节点开始调度更新 计划更新root
 * Scheduler模块 任务 优先级
 * @param {*} root
 * @param {*} fiber
 * @param {*} lane
 * @param {*} eventTime
 */
export function scheduleUpdateOnFiber(root, fiber, lane, eventTime) {
  // 1. 将root标记为更新 root.pendingLanes |= updateLane;
  markRootUpdated(root, lane, eventTime);
  if (root === workInProgressRoot) {
  }
  // 2.调度root
  // 2.1 lane优先级 newCallbackPriority
  // 2.2 将callback添加到 syncQueue中 scheduleSyncCallback(performSyncWorkOnRoot添加到queue中)
  // 2.3 在microtask中清空任务 queueMicrotask(flushSyncCallbacks)
  // 2.3.1 设置优先级
  // 2.3.2 依次执行 syncQueue中的回调 callback = callback(isSync);
  // 2.3.3 执行 performSyncWorkOnRoot
  ensureRootIsScheduled(root, eventTime);

  // 开发 ReactCurrentActQueue.isBatchingLegacy
  if (
    lane === SyncLane &&
    executionContext === NoContext &&
    (fiber.mode & ConcurrentMode) === NoMode
  ) {
    // resetRenderTimer();
    flushSyncCallbacksOnlyInLegacyMode();
  }
}

/**
 * 确保root节点的更新
 * @param {*} root
 * @param {*} currentTime
 */
function ensureRootIsScheduled(root, currentTime) {
  // 当前根上执行的任务
  const existingCallbackNode = root.callbackNode;
  // 饥饿问题 把饿死的赛道标记为过期
  markStarvedLanesAsExpired(root, currentTime);
  // root.pendingLanes 并发模式是normal的
  // getNextLanes => getHighestPriorityLanes(pendingLanes)
  // 获取pendingLanes上优先级最高的
  // 找到优先级最高的车道
  const nextLanes = getNextLanes(
    root,
    // workInProgressRootRenderLanes 当前正在渲染的车道
    root === workInProgressRoot ? workInProgressRootRenderLanes : NoLanes
  );
  // 如果没有任务执行就直接return
  if (nextLanes === NoLanes) {
    if (existingCallbackNode !== null) {
      Scheduler_cancelCallback(existingCallbackNode);
    }
    root.callbackNode = null;
    root.callbackPriority = NoLane;
    return;
  }
  const newCallbackPriority = getHighestPriorityLane(nextLanes);
  // ReactDOM.render() 的priority为SyncLane同步的模式
  // createRoot的是并发模式的
  // 跟上正在运行的优先级
  const existingCallbackPriority = root.callbackPriority;
  //  优先级相同的任务 直接 return 批量更新
  if (newCallbackPriority === existingCallbackPriority) return;
  if (existingCallbackNode !== null) {
    // Cancel the existing callback. We'll schedule a new one below.
    // cancelCallback(existingCallbackNode);
    // 取消任务
    Scheduler_cancelCallback(existingCallbackNode);
  }
  let newCallbackNode;
  // 同步的模式
  if (includesSyncLane(newCallbackPriority)) {
    // 当点击事件发生更新的时候 是同步的更新的
    console.log("同步的模式");
    // 添加到同步队列中
    if (root.tag === LegacyRoot) {
      // 将 callback 添加到 syncQueue 中
      scheduleLegacySyncCallback(performSyncWorkOnRoot.bind(null, root));
    } else {
      scheduleSyncCallback(performSyncWorkOnRoot.bind(null, root));
    }
    if (supportsMicrotasks) {
      scheduleMicrotask(() => {
        if (
          (executionContext & (RenderContext | CommitContext)) ===
          NoContext
        ) {
          flushSyncCallbacks();
        }
      });
    } else {
      // Scheduler 调度模块
      // 调度的优先级 对应不同的 timeout 和react的调度是分开的
      // 事件任务优先级 => lane模型的优先级 => scheduler调度的优先级
      Scheduler_scheduleCallback(
        ImmediateSchedulerPriority,
        flushSyncCallbacks
      );
    }
    // 同步任务就是null
    newCallbackNode = null;
  } else {
    console.log("并发模式");
    // 并发模式
    // 调度的优先级 并发渲染的优先级是
    let schedulerPriorityLevel;
    // lane对应事件优先级 默认的
    // 调度的优先级是 0 1 2 3 4 5
    // 将车道变成事件的优先级
    switch (lanesToEventPriority(nextLanes)) {
      // 变成调度的优先级
      case DefaultEventPriority:
        schedulerPriorityLevel = NormalSchedulerPriority;
        break;
      default:
        schedulerPriorityLevel = NormalSchedulerPriority;
        break;
    }
    // 返回一个值 返回一个 newTask
    newCallbackNode = Scheduler_scheduleCallback(
      schedulerPriorityLevel,
      performConcurrentWorkOnRoot.bind(null, root)
    );
  }
  root.callbackPriority = newCallbackPriority;
  root.callbackNode = newCallbackNode;
}

// 在微任务中清空 syncQueue callback()
function performSyncWorkOnRoot(root) {
  // useEffect
  // flushPassiveEffects();
  // 拿到最高优先级的赛道
  let lanes = getNextLanes(root, NoLanes);

  // workLoopSync
  const exitStatus = renderRootSync(root, lanes);
  // 判断退出的状态
  const finishedWork = root.current.alternate;
  root.finishedWork = finishedWork;
  // root.finishedLanes = lanes;
  // 提交
  commitRoot(root);
  // ensureRootIsScheduled(root, now());
  return null;
}

/**
 * workLoop 开始构建fiber树 workLoopSync
 * @param {*} root
 * @param {*} lanes
 * @returns
 */
function renderRootSync(root, lanes) {
  const prevExecutionContext = executionContext;
  // 当前执行上下文为渲染
  executionContext |= RenderContext;
  // 新的渲染优先级和老的渲染优先级不一样 就会创建新的
  if (workInProgressRoot !== root || workInProgressRootRenderLanes !== lanes) {
    // 1. 创建 workInProgress createWorkInProgress()
    // 有替身就两个换 没有就创建正在工作的fiber对象
    prepareFreshStack(root, lanes);
  }
  do {
    try {
      // if(workInProgress !== null) {}
      // 2. 开始工作循环
      workLoopSync();
      break;
    } catch (thrownValue) {
      console.log(thrownValue);
    }
  } while (true);
  executionContext = prevExecutionContext;
  workInProgressRoot = null;
  workInProgressRootRenderLanes = NoLanes;
  finishQueueingConcurrentUpdates();
  // 并发模式下的退出状态
  return workInProgressRootExitStatus;
}

// 根据老fiber和属性创建新的fiber
// workInProgress 和 rootWorkInProgress 区别
function prepareFreshStack(root, lanes) {
  // resetWorkInProgressStack();
  workInProgressRoot = root;
  // 创建 FiberNode
  const rootWorkInProgress = createWorkInProgress(root.current, null);
  workInProgressRootExitStatus = RootInProgress;
  workInProgress = rootWorkInProgress;
  // 渲染的优先级
  workInProgressRootRenderLanes = renderLanes = lanes;
  // 构建更新的循环链表 先一批批的放在数组中的
  finishQueueingConcurrentUpdates();
  return rootWorkInProgress;
}

function workLoopSync() {
  while (workInProgress !== null) {
    performUnitOfWork(workInProgress);
  }
}

function performUnitOfWork(unitOfWork) {
  // console.log("performUnitOfWork", unitOfWork);
  // current对应现在试图的fiber workInProgress是正在工作的fiber
  const current = unitOfWork.alternate;
  let next;
  // fiber开始工作 返回第一个儿子
  next = beginWork(current, unitOfWork, renderLanes);
  unitOfWork.memoizedProps = unitOfWork.pendingProps;
  if (next === null) {
    // 如果没有了就表示自己完成了
    completeUnitOfWork(unitOfWork);
  } else {
    workInProgress = next;
  }
  // ReactCurrentOwner.current = null;
}

/**
 * fiber没有子节点就完成了
 * @param {*} unitOfWork
 * @returns
 */
function completeUnitOfWork(unitOfWork) {
  let completedWork = unitOfWork;
  do {
    const current = completedWork.alternate;
    const returnFiber = completedWork.return;
    let next;
    // 完成了 执行fiber的完成工作
    // 如果是原生的就创建真实的dom节点
    // next = completeWork(current, completedWork, renderLanes);
    next = completeWork(current, completedWork);
    if (next !== null) {
      workInProgress = next;
      return;
    }
    // 如果这个节点return null 找弟弟
    const siblingFiber = completedWork.sibling;
    if (siblingFiber !== null) {
      workInProgress = siblingFiber;
      return;
    }
    // 如果弟弟也完成了 就表示让parent完成 同时找叔叔开始工作
    completedWork = returnFiber;
    // 这个很重要
    workInProgress = completedWork;
  } while (completedWork !== null);
  // 更改状态 这里表示整个fiber树构建完成了
  if (workInProgressRootExitStatus === RootInProgress) {
    workInProgressRootExitStatus = RootCompleted;
  }
}

// 提交根节点
function commitRoot(root, recoverableErrors, transitions) {
  const previousUpdateLanePriority = getCurrentUpdatePriority();
  try {
    // commit阶段是不可中断的 要一气呵成的干完
    setCurrentUpdatePriority(DiscreteEventPriority);
    commitRootImpl(
      root,
      recoverableErrors,
      transitions,
      previousUpdateLanePriority
    );
  } finally {
    setCurrentUpdatePriority(previousUpdateLanePriority);
  }
  return null;
}

function commitRootImpl(
  root,
  recoverableErrors,
  transitions,
  renderPriorityLevel
) {
  const finishedWork = root.finishedWork;
  do {
    flushPassiveEffects();
  } while (rootWithPendingPassiveEffects !== null);
  // 如果自己有 Passive 或者child有Passive 表示有需要执行的
  if (
    (finishedWork.subtreeFlags & PassiveMask) !== NoFlags ||
    (finishedWork.flags & PassiveMask) !== NoFlags
  ) {
    if (!rootDoesHavePassiveEffects) {
      // 开始根上有
      rootDoesHavePassiveEffects = true;
      // 添加到调度中 NormalSchedulerPriority 优先级
      scheduleCallback(NormalSchedulerPriority, () => {
        flushPassiveEffects();
        return null;
      });
    }
  }

  const lanes = root.finishedLanes;
  root.finishedWork = null;
  root.finishedLanes = NoLanes;
  root.callbackNode = null;
  root.callbackPriority = NoLane;
  // 统计当前新的根上剩下的车道
  let remainingLanes = mergeLanes(finishedWork.lanes, finishedWork.childLanes);
  // const concurrentlyUpdatedLanes = getConcurrentlyUpdatedLanes();
  // remainingLanes = mergeLanes(remainingLanes, concurrentlyUpdatedLanes);
  // 标记根为完成
  markRootFinished(root, remainingLanes);
  if (root === workInProgressRoot) {
    workInProgressRoot = null;
    workInProgress = null;
    workInProgressRootRenderLanes = NoLanes;
  } else {
  }
  // 子节点有副作用 subtreeFlags
  const subtreeHasEffects =
    (finishedWork.subtreeFlags &
      (BeforeMutationMask | MutationMask | LayoutMask | PassiveMask)) !==
    NoFlags;
  // 根fiber自己的副作用 flags
  const rootHasEffect =
    (finishedWork.flags &
      (BeforeMutationMask | MutationMask | LayoutMask | PassiveMask)) !==
    NoFlags;
  // 有副作用就提交dom操作
  if (subtreeHasEffects || rootHasEffect) {
    // const prevExecutionContext = executionContext;
    // // 提交阶段
    // executionContext |= CommitContext;
    // 1. before mutation dom变更前
    // const shouldFireAfterActiveInstanceBlur = commitBeforeMutationEffects(
    //   root,
    //   finishedWork
    // );

    // 2. mutation 提交阶段 dom变更
    commitMutationEffects(root, finishedWork, lanes);

    // dom变更完成之后 current指向新的根fiber
    root.current = finishedWork;

    // 3. 提交useLayoutEffect的 dom变更之后
    commitLayoutEffects(finishedWork, root, lanes);

    // commit完成之后

    // unstable_requestPaint 调度模块
    // requestPaint();
    // executionContext = prevExecutionContext;
  } else {
    root.current = finishedWork;
  }

  if (rootDoesHavePassiveEffects) {
    rootDoesHavePassiveEffects = false;
    rootWithPendingPassiveEffects = root;
    // pendingPassiveEffectsLanes = lanes;
  }
  remainingLanes = root.pendingLanes;
  // 提交完之后要再次调度 根上可能会有跳过的更新
  ensureRootIsScheduled(root, now());
  flushSyncCallbacks();
  return null;
}

/**
 * 开始工作循环
 * @param {*} root
 * @param {*} didTimeout
 */
function performConcurrentWorkOnRoot(root, didTimeout) {
  // console.log("performConcurrentWorkOnRoot");
  // Scheduler_scheduleCallback 返回值 是一个task任务 return newTask
  // 1. 获取根节点上的任务
  const originalCallbackNode = root.callbackNode;
  // 2. 拿到车道
  let lanes = getNextLanes(
    root,
    root === workInProgressRoot ? workInProgressRootRenderLanes : NoLanes
  );
  if (lanes === NoLanes) {
    return null;
  }
  // shouldTimeSlice 时间切片
  // allowConcurrentByDefault 是一个开关
  // const nonIncludesBlockingLane = !includesBlockingLane(root, lanes);
  // const nonIncludesExpiredLane = !includesExpiredLane(root, lanes);
  // const nonTimeout = disableSchedulerTimeoutInWorkLoop || !didTimeout;
  const shouldTimeSlice =
    // 没有阻塞的车道
    !includesBlockingLane(root, lanes) &&
    // 不包含过期的 如果有过期的就不能并发渲染 就要同步执行
    !includesExpiredLane(root, lanes) &&
    // disableSchedulerTimeoutInWorkLoop = false
    (disableSchedulerTimeoutInWorkLoop || !didTimeout);
  // console.log(shouldTimeSlice, "shouldTimeSlice");
  // 初次渲染是同步模式 第一次渲染肯定是同步的

  let exitStatus = shouldTimeSlice
    ? renderRootConcurrent(root, lanes)
    : renderRootSync(root, lanes);
  // ensureRootIsScheduled(root, now());
  if (exitStatus !== RootInProgress) {
    if (exitStatus === RootDidNotComplete) {
    } else {
      const finishedWork = root.current.alternate;
      root.finishedWork = finishedWork;
      root.finishedLanes = lanes;
      // 结束 commitRoot()
      finishConcurrentRender(root, exitStatus, lanes);
    }
  }
  // 说明任务还没有完成 提交阶段会清空
  if (root.callbackNode === originalCallbackNode) {
    // 下次继续干
    return performConcurrentWorkOnRoot.bind(null, root);
  }
  return null;
}

// render阶段结束了 根据不同的exitStatus状态处理
function finishConcurrentRender(root, exitStatus, lanes) {
  switch (exitStatus) {
    case RootCompleted: {
      // 提交
      commitRoot(root);
      break;
    }
    default: {
      throw new Error("Unknown root exit status.");
    }
  }
}

function renderRootConcurrent(root, lanes) {
  // const prevExecutionContext = executionContext;
  // executionContext |= RenderContext;
  // 在构建fiber树的过程中这个方法会多次的执行 不是每次都创建的
  if (workInProgressRoot !== root || workInProgressRootRenderLanes !== lanes) {
    prepareFreshStack(root, lanes);
  }
  // 指向工作循环 在当前分配的5ms中执行
  workLoopConcurrent();
  if (workInProgress !== null) {
    // 还没有完成就返回构建中 继续工作
    return RootInProgress;
  } else {
    workInProgressRoot = null;
    workInProgressRootRenderLanes = NoLanes;
    finishQueueingConcurrentUpdates();
    // 完成工作的时候就修改为5
    return workInProgressRootExitStatus;
  }
}

function workLoopConcurrent() {
  while (workInProgress !== null && !shouldYield()) {
    // 测试分片 这样每次都是timeout
    // sleep(6);
    // 测试高优任务打断低优先任务
    // sleep(100);
    performUnitOfWork(workInProgress);
  }
}

// 批量更新
export function batchedUpdates(fn, a) {
  const prevExecutionContext = executionContext;
  executionContext |= BatchedContext;
  try {
    fn(a);
  } finally {
    executionContext = prevExecutionContext;
  }
}

export function setRenderLanes(subtreeRenderLanes) {
  renderLanes = subtreeRenderLanes;
}

export function getRenderLanes() {
  return renderLanes;
}

export function flushPassiveEffects() {
  if (rootWithPendingPassiveEffects !== null) {
    // 有需要提交的
    const root = rootWithPendingPassiveEffects;
    return flushPassiveEffectsImpl(root.current);
  }
  return false;
}

function flushPassiveEffectsImpl() {
  if (rootWithPendingPassiveEffects === null) return false;
  const root = rootWithPendingPassiveEffects;
  // const lanes = pendingPassiveEffectsLanes;
  rootWithPendingPassiveEffects = null;
  // pendingPassiveEffectsLanes = NoLanes;

  // 先执行 destroy函数的
  commitPassiveUnmountEffects(root.current);
  // commitPassiveMountEffects(root, root.current, lanes, transitions);
  commitPassiveMountEffects(root, root.current);

  // const prevExecutionContext = executionContext;
  // executionContext |= CommitContext;
  // executionContext = prevExecutionContext; // 还原
  // flushSyncCallbacks();
  return true;
}

export function getWorkInProgressRootRenderLanes() {
  return workInProgressRootRenderLanes;
}

// 用来测试
function sleep(time) {
  const timeStamp = new Date().getTime();
  const endTime = timeStamp + time;
  while (true) {
    if (new Date().getTime() > endTime) {
      return;
    }
  }
}
