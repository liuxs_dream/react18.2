// hook的类型
export const NoFlags = 0b0000;
// 有effect
export const HasEffect = 0b0001; // 1
export const Insertion = 0b0010;
// layout
export const Layout = 0b0100; // 4
// effect
export const Passive = 0b1000; // 8
