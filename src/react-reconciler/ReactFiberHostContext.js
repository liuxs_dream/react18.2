import { createCursor } from "./ReactFiberStack";

const rootInstanceStackCursor = createCursor(null);
const contextStackCursor = createCursor(null);

function requiredContext(c) {
  return c;
}

export function getRootHostContainer() {
  // {current: null}
  const rootInstance = requiredContext(rootInstanceStackCursor.current);
  return rootInstance;
}

export function getHostContext() {
  // {current: null}
  const context = requiredContext(contextStackCursor.current);
  return context;
}
