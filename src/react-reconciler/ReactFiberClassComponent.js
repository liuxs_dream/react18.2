import {
  createUpdate,
  initializeUpdateQueue,
  cloneUpdateQueue,
  processUpdateQueue,
  enqueueUpdate,
} from "./ReactFiberClassUpdateQueue";
import {
  get as getInstance,
  set as setInstance,
} from "../shared/ReactInstanceMap";
import { isMounted } from "./ReactFiberTreeReflection";
import {
  requestEventTime,
  requestUpdateLane,
  scheduleUpdateOnFiber,
} from "./ReactFiberWorkLoop";
import { Update, Snapshot } from "./ReactFiberFlags";
import { emptyContextObject } from "./ReactFiberContext";

export function constructClassInstance(workInProgress, ctor, props) {
  let context = emptyContextObject;
  let instance = new ctor(props, context);
  adoptClassInstance(workInProgress, instance);
  return instance;
}

const classComponentUpdater = {
  isMounted,
  enqueueSetState(inst, payload, callback) {
    const fiber = getInstance(inst);
    const eventTime = requestEventTime();
    const lane = requestUpdateLane(fiber);
    const update = createUpdate(eventTime, lane);
    update.payload = payload;
    const root = enqueueUpdate(fiber, update, lane);
    if (root !== null) {
      scheduleUpdateOnFiber(root, fiber, lane, eventTime);
    }
  },
};

function adoptClassInstance(workInProgress, instance) {
  instance.updater = classComponentUpdater;
  workInProgress.stateNode = instance;
  // key._reactInternals = value;
  setInstance(instance, workInProgress);
}
/**
 * 挂载
 * @param {*} workInProgress
 * @param {*} ctor
 * @param {*} newProps
 * @param {*} renderLanes
 */
export function mountClassInstance(
  workInProgress,
  ctor,
  newProps,
  renderLanes
) {
  // const instance = workInProgress.stateNode;
  // instance.props = newProps;
  // instance.state = workInProgress.memoizedState;
  // instance.refs = {};

  //  初始化更新队列
  initializeUpdateQueue(workInProgress);
  // instance.state = workInProgress.memoizedState;
  // 处理 context
  // const contextType = ctor.contextType;
  // 生命周期函数
  // const getDerivedStateFromProps = ctor.getDerivedStateFromProps;
  // if (typeof instance.componentDidMount === 'function') {}
}

// 更新
export function updateClassInstance(
  current,
  workInProgress,
  ctor,
  newProps,
  renderLanes
) {
  const instance = workInProgress.stateNode;
  // 更新队列 为什么要克隆
  cloneUpdateQueue(current, workInProgress);

  // 1. props
  // memoizedProps props pendingProps 区别???
  const unresolvedOldProps = workInProgress.memoizedProps;
  const oldProps = unresolvedOldProps;
  instance.props = oldProps;
  const unresolvedNewProps = workInProgress.pendingProps;

  // 2. context
  const oldContext = instance.context;
  const contextType = ctor.contextType;
  let nextContext = emptyContextObject;
  // getDerivedStateFromProps

  // 3. state
  const oldState = workInProgress.memoizedState;
  let newState = (instance.state = oldState);
  // 处理更新队列 更新state
  processUpdateQueue(workInProgress, newProps, instance, renderLanes);
  newState = workInProgress.memoizedState;
  // getDerivedStateFromProps
  const shouldUpdate = true;
  if (shouldUpdate) {
    if (typeof instance.componentDidUpdate === "function") {
      workInProgress.flags |= Update;
    }
    if (typeof instance.getSnapshotBeforeUpdate === "function") {
      workInProgress.flags |= Snapshot;
    }
  } else {
    // 生命周期钩子函数
    // shouldUpdate
    // componentDidUpdate
    // getSnapshotBeforeUpdate
    // workInProgress.memoizedProps = newProps;
    // workInProgress.memoizedState = newState;
  }
  // 无论是否需要更新都更新状态
  instance.props = newProps;
  instance.state = newState;
  // instance.context = nextContext;
  return shouldUpdate;
}
