export function createCursor(defaultValue) {
  return { current: defaultValue };
}
