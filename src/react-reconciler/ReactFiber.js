import { ConcurrentRoot } from "./ReactRootTags";
import {
  ConcurrentMode,
  ConcurrentUpdatesByDefaultMode,
  NoMode,
} from "./ReactTypeOfMode";
import { NoFlags } from "./ReactFiberFlags";
import { NoLanes } from "./ReactFiberLane";
import {
  enableSyncDefaultUpdates,
  allowConcurrentByDefault,
} from "../shared/ReactFeatureFlags";
import {
  ClassComponent,
  HostComponent,
  HostRoot,
  HostText,
  IndeterminateComponent,
} from "./ReactWorkTags";
import { ContextProvider, ContextConsumer } from "./ReactWorkTags";
import {
  REACT_PROVIDER_TYPE,
  REACT_CONTEXT_TYPE,
} from "../shared/ReactSymbols";

export function createHostRootFiber(
  tag,
  isStrictMode,
  concurrentUpdatesByDefaultOverride
) {
  // react通过mode(tag标签)来区分是并发渲染还是传统的
  let mode;
  if (tag === ConcurrentRoot) {
    mode = ConcurrentMode;
    if (
      !enableSyncDefaultUpdates ||
      (allowConcurrentByDefault && concurrentUpdatesByDefaultOverride)
    ) {
      mode |= ConcurrentUpdatesByDefaultMode;
    }
  } else {
    mode = NoMode;
  }
  return createFiber(HostRoot, null, null, mode);
}

// 创建一个fiber节点
const createFiber = function (tag, pendingProps, key, mode) {
  return new FiberNode(tag, pendingProps, key, mode);
};

/**
 *
 * @param {*} tag fiber的类型 HostRoot FunctionComponent
 * @param {*} pendingProps 等待生效的属性
 * @param {*} key 唯一标识
 * @param {*} mode
 */
function FiberNode(tag, pendingProps, key, mode) {
  this.tag = tag; // fiber的类型
  this.key = key; // 唯一标识
  this.type = null; // fiber的类型 虚拟dom 的节点类型 span div
  this.stateNode = null; // fiber对应的真实的dom节点

  // fiber 的数据结构
  this.return = null; // 父节点
  this.sibling = null; // 弟弟
  this.child = null; // 大儿子

  this.ref = null; // 引用 useRef

  // 通过虚拟dom创建fiber 虚拟dom会提供pendingProps来创建fiber的属性
  // 处理完之后就会赋值给 memoizedProps
  this.pendingProps = pendingProps; // 待生效的属性
  this.memoizedProps = null; // 已经生效的属性
  // 函数组件的指向 effectHook构建的effect循环链表
  this.updateQueue = null; // 更新队列 循环链表

  // 每个fiber都有一个状态 每个fiber都不同
  // 类组件村的是实例的状态
  // hostRoot存放的是渲染的元素
  // 函数组件的 memoizedState 指向的是hook形成的单链表
  // 每个hook中有不同的 memoizedState
  // useState对应的memoizedState就是内部的state状态
  // useEffect的memoizedState指向的是 effect的循环链表
  this.memoizedState = null;

  // 模型 并发模型 传统模型
  this.mode = mode;

  this.flags = NoFlags; // 副作用标识 针对此fiber节点要进行的副作用操作 dom变更
  // 在react进行dom-diff的过程中要计算要执行的操作 使用位运算
  // react18 去掉了 effectList 不在收集了
  // 儿子的副作用会向上冒泡给父节点
  this.subtreeFlags = NoFlags; // 子节点对应的副作用标识 和自己的flags是独立的
  this.deletions = null; // 要删除的

  // lane模型 优先级相关
  this.lanes = NoLanes;
  this.childLanes = NoLanes;

  // 双缓存模型 最多只有两个 fiber树
  // dom-diff
  this.alternate = null;
}

/**
 * 1. workInProgress 和 current不是一个对象
 * 2. workInProgress两种情况
 * 2.1 复用老的fiber对象
 * 2.2 复用真实的dom节点
 * @param {*} current
 * @param {*} pendingProps
 * @returns
 */
export function createWorkInProgress(current, pendingProps) {
  let workInProgress = current.alternate;
  if (workInProgress === null) {
    // 没有替身就创建
    workInProgress = createFiber(
      current.tag,
      pendingProps,
      current.key,
      current.mode
    );
    workInProgress.elementType = current.elementType;
    workInProgress.type = current.type;
    workInProgress.stateNode = current.stateNode;
    // 相互指向
    workInProgress.alternate = current;
    current.alternate = workInProgress;
  } else {
    // 有就复用 双缓存
    workInProgress.pendingProps = pendingProps;
    workInProgress.type = current.type;
    workInProgress.flags = NoFlags;
    workInProgress.subtreeFlags = NoFlags;
    workInProgress.deletions = null;
  }
  // 重置 effects
  workInProgress.flags = current.flags;
  workInProgress.childLanes = current.childLanes;
  workInProgress.lanes = current.lanes;

  workInProgress.child = current.child;
  workInProgress.memoizedProps = current.memoizedProps;
  workInProgress.memoizedState = current.memoizedState;
  workInProgress.updateQueue = current.updateQueue;
  workInProgress.ref = current.ref; // ref
  return workInProgress;
}

// 文本fiber
export function createFiberFromText(content, mode, lanes) {
  const fiber = createFiber(HostText, content, null, mode);
  fiber.lanes = lanes;
  return fiber;
}

// 根据虚拟dom节点创建fiber节点
export function createFiberFromElement(element, mode, lanes) {
  const type = element.type;
  const key = element.key;
  const pendingProps = element.props;
  // 创建fiber
  const fiber = createFiberFromTypeAndProps(
    type,
    key,
    pendingProps,
    mode,
    lanes
  );
  return fiber;
}

function createFiberFromTypeAndProps(type, key, pendingProps, mode, lanes) {
  // 开始时不知道类型的
  let fiberTag = IndeterminateComponent;
  if (typeof type === "function") {
    if (shouldConstruct(type)) {
      // isReactComponent
      fiberTag = ClassComponent;
    }
  } else if (typeof type === "string") {
    // div span 原生组件
    fiberTag = HostComponent;
  } else {
    if (typeof type === "object" && type !== null) {
      switch (type.$$typeof) {
        case REACT_PROVIDER_TYPE:
          fiberTag = ContextProvider;
          break;
        case REACT_CONTEXT_TYPE:
          fiberTag = ContextConsumer;
          break;
      }
    }
  }
  const fiber = createFiber(fiberTag, pendingProps, key, mode);
  fiber.elementType = type;
  fiber.type = type;
  fiber.lanes = lanes;
  return fiber;
}

function shouldConstruct(Component) {
  const prototype = Component.prototype;
  return !!(prototype && prototype.isReactComponent);
}
