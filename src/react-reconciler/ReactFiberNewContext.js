import { createCursor } from "./ReactFiberStack";
import { enableServerContext } from "../shared/ReactFeatureFlags";
import { REACT_SERVER_CONTEXT_DEFAULT_VALUE_NOT_LOADED } from "../shared/ReactSymbols";

export const isPrimaryRenderer = true;
// {current: defaultValue}
const valueCursor = createCursor(null);
const valueStack = [];
let index = -1;

export function pushProvider(providerFiber, context, nextValue) {
  push(valueCursor, context._currentValue, providerFiber);

  context._currentValue = nextValue;
}

export function readContext(context) {
  const value = context._currentValue;
  return value;
}

export function popProvider(context, providerFiber) {
  const currentValue = valueCursor.current;
  pop(valueCursor, providerFiber);
  if (
    enableServerContext &&
    currentValue === REACT_SERVER_CONTEXT_DEFAULT_VALUE_NOT_LOADED
  ) {
    context._currentValue = context._defaultValue;
  } else {
    context._currentValue = currentValue;
  }
}

function pop(cursor, fiber) {
  if (index < 0) {
    return;
  }
  cursor.current = valueStack[index];
  valueStack[index] = null;
  index--;
}

function push(cursor, value, fiber) {
  index++;
  valueStack[index] = cursor.current;
  cursor.current = value;
}
