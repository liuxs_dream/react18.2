import {
  NoLanes,
  includesNonIdleWork,
  includesSomeLane,
} from "./ReactFiberLane";
import {
  ClassComponent,
  ContextProvider,
  ContextConsumer,
  FunctionComponent,
  HostComponent,
  HostRoot,
  HostText,
  IndeterminateComponent,
} from "./ReactWorkTags";
import {
  cloneUpdateQueue,
  processUpdateQueue,
} from "./ReactFiberClassUpdateQueue";
import {
  reconcileChildFibers,
  mountChildFibers,
  cloneChildFibers,
} from "./ReactChildFiber";
import { ContentReset, PerformedWork } from "./ReactFiberFlags";
import { shouldSetTextContent } from "../react-dom-bindings/client/ReactDOMHostConfig";
import { renderWithHooks } from "./ReactFiberHooks";
import {
  mountClassInstance,
  constructClassInstance,
  updateClassInstance,
} from "./ReactFiberClassComponent";
import { pushProvider } from "./ReactFiberNewContext";

/**
 * 根据新的虚拟dom构建新的fiber链表
 * @param {*} current
 * @param {*} workInProgress
 * @param {*} renderLanes
 * @returns
 */
export function beginWork(current, workInProgress, renderLanes) {
  if (current !== null) {
    // 更新的时候 current不是null
    const oldProps = current.memoizedProps;
    const newProps = workInProgress.pendingProps;
    if (oldProps !== newProps) {
    } else {
      const hasScheduledUpdateOrContext = checkScheduledUpdateOrContext(
        current,
        renderLanes
      );
      if (!hasScheduledUpdateOrContext) {
        // root上没有
        return attemptEarlyBailoutIfNoScheduledUpdate(
          current,
          workInProgress,
          renderLanes
        );
      }
    }
  } else {
  }
  // 在构建fiber树之前先清空lanes
  workInProgress.lanes = NoLanes;
  // 判断  workInProgress的tag
  switch (workInProgress.tag) {
    // 第一次进来是hostRoot
    case HostRoot: {
      return updateHostRoot(current, workInProgress, renderLanes);
    }
    case HostComponent: {
      // 原生dom节点
      return updateHostComponent(current, workInProgress, renderLanes);
    }
    case HostText: {
      // 文本节点 初次渲染文本节点的时候 root返回的child是textFiber
      // return null 完成这个节点
      return updateHostText(current, workInProgress);
    }
    case IndeterminateComponent: {
      // 组件开始是不知道类型的
      return mountIndeterminateComponent(
        current,
        workInProgress,
        workInProgress.type,
        renderLanes
      );
    }
    // 在处理root节点的child时 createFiberFromTypeAndProps
    // 如果是类组件就会将tag修改为ClassComponent
    case ClassComponent: {
      const Component = workInProgress.type;
      const resolvedProps = workInProgress.pendingProps;
      return updateClassComponent(
        current,
        workInProgress,
        Component,
        resolvedProps,
        renderLanes
      );
    }
    // 函数组件
    // 在执行IndeterminateComponent的时候会将tag修改为函数组件 更新的时候就会执行这里了
    case FunctionComponent: {
      const Component = workInProgress.type;
      const unresolvedProps = workInProgress.pendingProps;
      const resolvedProps = unresolvedProps;
      return updateFunctionComponent(
        current,
        workInProgress,
        Component,
        resolvedProps,
        renderLanes
      );
    }
    case ContextProvider:
      return updateContextProvider(current, workInProgress, renderLanes);
    case ContextConsumer:
      return updateContextConsumer(current, workInProgress, renderLanes);
    default:
      // break;
      return null;
  }
}

// 处理根节点
function updateHostRoot(current, workInProgress, renderLanes) {
  const nextProps = workInProgress.pendingProps;
  const prevState = workInProgress.memoizedState;
  const prevChildren = prevState.element;
  // 克隆更新队列 queue === currentQueue
  // 为什么要克隆 workInProgress.updateQueue = clone;
  cloneUpdateQueue(current, workInProgress);
  // 处理更新队列 循环链表断开 计算 newState
  processUpdateQueue(workInProgress, nextProps, null, renderLanes);
  // {element: xxx}
  const nextState = workInProgress.memoizedState;
  // containerInfo: {div#root}
  const root = workInProgress.stateNode;
  const nextChildren = nextState.element;
  // 协调孩子 DOM-DIFF
  reconcileChildren(current, workInProgress, nextChildren, renderLanes);
  return workInProgress.child;
}

// 处理文本
function updateHostText() {
  return null;
}

// 原生dom节点
function updateHostComponent(current, workInProgress, renderLanes) {
  const type = workInProgress.type; // div
  // 新的属性
  const nextProps = workInProgress.pendingProps;
  const prevProps = current !== null ? current.memoizedProps : null;
  // 虚拟dom节点
  let nextChildren = nextProps.children;
  // 如果只是一个文本节点是不会创建fiber的 props.children是string或者number
  const isDirectTextChild = shouldSetTextContent(type, nextProps);
  if (isDirectTextChild) {
    nextChildren = null;
  } else if (prevProps !== null && shouldSetTextContent(type, prevProps)) {
    // 内容重置 更新
    workInProgress.flags |= ContentReset;
  }
  reconcileChildren(current, workInProgress, nextChildren, renderLanes);
  return workInProgress.child;
}

export function reconcileChildren(
  current,
  workInProgress,
  nextChildren,
  renderLanes
) {
  if (current === null) {
    // ReactChildFiber.js
    workInProgress.child = mountChildFibers(
      workInProgress,
      null,
      nextChildren,
      renderLanes
    );
  } else {
    // 根节点的current是存在的
    workInProgress.child = reconcileChildFibers(
      workInProgress,
      current.child,
      nextChildren,
      renderLanes
    );
  }
}

//
function updateClassComponent(
  current,
  workInProgress,
  Component,
  nextProps,
  renderLanes
) {
  const instance = workInProgress.stateNode;
  let shouldUpdate;
  let hasContext;
  hasContext = false;
  if (instance === null) {
    // 初次挂载
    // stateNode赋值了
    constructClassInstance(workInProgress, Component, nextProps);
    mountClassInstance(workInProgress, Component, nextProps, renderLanes);
    shouldUpdate = true;
  } else if (current === null) {
    // resume
  } else {
    // 更新
    shouldUpdate = updateClassInstance(
      current,
      workInProgress,
      Component,
      nextProps,
      renderLanes
    );
  }
  // 执行render函数得到虚拟dom节点
  const nextUnitOfWork = finishClassComponent(
    current,
    workInProgress,
    Component,
    shouldUpdate,
    hasContext,
    renderLanes
  );

  return nextUnitOfWork;
}

function finishClassComponent(
  current,
  workInProgress,
  Component,
  shouldUpdate,
  hasContext,
  renderLanes
) {
  const instance = workInProgress.stateNode;
  let nextChildren;
  // 执行render函数
  nextChildren = instance.render();
  reconcileChildren(current, workInProgress, nextChildren, renderLanes);
  workInProgress.flags |= PerformedWork;
  workInProgress.memoizedState = instance.state;
  return workInProgress.child;
}

/**
 * 未知的组件类型 函数组件第一次挂载的时候
 * @param {*} _current
 * @param {*} workInProgress
 * @param {*} Component
 * @param {*} renderLanes
 */
function mountIndeterminateComponent(
  _current,
  workInProgress,
  Component,
  renderLanes
) {
  const props = workInProgress.pendingProps;
  let context;
  let value;
  // 函数组件就是执行这个
  // value = Component(props) 返回 children
  value = renderWithHooks(
    null,
    workInProgress,
    Component,
    props,
    context,
    renderLanes
  );
  if (
    typeof value === "object" &&
    value !== null &&
    typeof value.render === "function" &&
    value.$$typeof === undefined
  ) {
    // 类组件
    workInProgress.tag = ClassComponent;
    // mountClassInstance(workInProgress, Component, props, renderLanes);
  } else {
    // 函数组件
    workInProgress.tag = FunctionComponent;
    reconcileChildren(null, workInProgress, value, renderLanes);
    return workInProgress.child;
  }
}

// 更新函数组件
function updateFunctionComponent(
  current,
  workInProgress,
  Component,
  nextProps,
  renderLanes
) {
  let nextChildren;
  let context;
  // 渲染
  nextChildren = renderWithHooks(
    current,
    workInProgress,
    Component,
    nextProps,
    context,
    renderLanes
  );
  // workInProgress.flags |= PerformedWork;
  // 协调孩子
  reconcileChildren(current, workInProgress, nextChildren, renderLanes);
  return workInProgress.child;
}

function checkScheduledUpdateOrContext(current, renderLanes) {
  const updateLanes = current.lanes;
  if (includesNonIdleWork(updateLanes, renderLanes)) {
    return true;
  }
  return false;
}

function attemptEarlyBailoutIfNoScheduledUpdate(
  current,
  workInProgress,
  renderLanes
) {
  // // 也是判断不同的tag
  // switch (workInProgress.tag) {
  //   case HostRoot: {
  //     // pushHostRootContext(workInProgress);
  //     const root = workInProgress.stateNode;
  //     // pushRootTransition(workInProgress, root, renderLanes);
  //     resetHydrationState();
  //     break;
  //   }
  //   case HostComponent: {
  //     pushHostContext(workInProgress);
  //     break;
  //   }
  //   case ClassComponent: {
  //     const Component = workInProgress.type;
  //     break;
  //   }
  // }
  return bailoutOnAlreadyFinishedWork(current, workInProgress, renderLanes);
}

function bailoutOnAlreadyFinishedWork(current, workInProgress, renderLanes) {
  if (current !== null) {
    workInProgress.dependencies = current.dependencies;
  }
  // if (!includesSomeLane(renderLanes, workInProgress.childLanes)) {
  //   return null;
  // }
  // 克隆 subtree
  cloneChildFibers(current, workInProgress);
  // 返回child 新的虚拟dom节点
  return workInProgress.child;
}

function updateContextConsumer() {}

function updateContextProvider(current, workInProgress, renderLanes) {
  const providerType = workInProgress.type;
  const context = providerType._context;
  const newProps = workInProgress.pendingProps;
  const oldProps = workInProgress.memoizedProps;
  const newValue = newProps.value;
  pushProvider(workInProgress, context, newValue);
  const newChildren = newProps.children;
  reconcileChildren(current, workInProgress, newChildren, renderLanes);
  return workInProgress.child;
}
