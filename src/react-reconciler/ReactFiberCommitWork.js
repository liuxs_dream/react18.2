import {
  ClassComponent,
  FunctionComponent,
  HostComponent,
  HostRoot,
  HostText,
} from "./ReactWorkTags";
import {
  MutationMask,
  Passive,
  Placement,
  Ref,
  Update,
  PassiveMask,
  LayoutMask,
} from "./ReactFiberFlags";
import {
  HasEffect as HookHasEffect,
  Passive as HookPassive,
  Layout as HookLayout,
  Insertion as HookInsertion,
} from "./ReactHookEffectTags";
import {
  insertInContainerBefore,
  appendChildToContainer,
  removeChild,
  removeChildFromContainer,
  commitTextUpdate,
  commitUpdate,
  insertBefore,
  appendChild,
  commitMount,
  getPublicInstance,
} from "../react-dom-bindings/client/ReactDOMHostConfig";

export function commitMutationEffects(root, finishedWork, committedLanes) {
  commitMutationEffectsOnFiber(finishedWork, root, committedLanes);
}

/**
 * 遍历fiber树
 * 提交fiber上的变更的副作用
 * @param {*} finishedWork
 * @param {*} root
 * @param {*} lanes
 * @returns
 */
function commitMutationEffectsOnFiber(finishedWork, root, lanes) {
  const current = finishedWork.alternate;
  const flags = finishedWork.flags;
  switch (finishedWork.tag) {
    case HostRoot:
      // 递归提交 先处理child
      recursivelyTraverseMutationEffects(root, finishedWork, lanes);
      // 在处理自己的
      commitReconciliationEffects(finishedWork);
      if (flags & Update) {
        if (current !== null) {
          const prevRootState = current.memoizedState;
        }
      }
      return;
    case HostComponent: {
      recursivelyTraverseMutationEffects(root, finishedWork, lanes);
      commitReconciliationEffects(finishedWork);
      // 处理ref React.useERef()
      // if (flags & Ref) {
      //   if (current !== null) {
      //     safelyDetachRef(current, current.return);
      //   }
      // }
      // 处理更新的
      if (flags & Update) {
        // 获取dom元素
        const instance = finishedWork.stateNode;
        if (instance !== null) {
          const newProps = finishedWork.memoizedProps;
          const oldProps = current !== null ? current.memoizedProps : newProps;
          const type = finishedWork.type;
          // updateHostComponent
          // const updatePayload = prepareUpdate() 差异数组
          const updatePayload = finishedWork.updateQueue;
          finishedWork.updateQueue = null;
          // update
          if (updatePayload !== null) {
            commitUpdate(
              instance,
              updatePayload,
              type,
              oldProps,
              newProps,
              finishedWork
            );
          }
        }
      }
      return;
    }
    case HostText: {
      recursivelyTraverseMutationEffects(root, finishedWork, lanes);
      commitReconciliationEffects(finishedWork);
      if (flags & Update) {
        const textInstance = finishedWork.stateNode;
        const newText = finishedWork.memoizedProps;
        const oldText = current !== null ? current.memoizedProps : newText;
        // 更新 textInstance.nodeValue = newText;
        commitTextUpdate(textInstance, oldText, newText);
      }
      return;
    }
    case FunctionComponent: {
      recursivelyTraverseMutationEffects(root, finishedWork);
      commitReconciliationEffects(finishedWork);
      // 更新的时候执行 effect的钩子函数
      if (flags & Update) {
        // commitHookEffectListUnmount(
        //   HookInsertion | HookHasEffect,
        //   finishedWork,
        //   finishedWork.return
        // );
        // commitHookEffectListMount(HookInsertion | HookHasEffect, finishedWork);
        commitHookEffectListUnmount(
          HookLayout | HookHasEffect,
          finishedWork,
          finishedWork.return
        );
      }
      return;
    }
    case ClassComponent: {
      recursivelyTraverseMutationEffects(root, finishedWork);
      commitReconciliationEffects(finishedWork);
      // if (flags & Ref) {
      //   if (current !== null) {
      //     safelyDetachRef(current, current.return);
      //   }
      // }
      return;
    }
    default:
      recursivelyTraverseMutationEffects(root, finishedWork, lanes);
      commitReconciliationEffects(finishedWork);
      break;
  }
}

function recursivelyTraverseMutationEffects(root, parentFiber, lanes) {
  const deletions = parentFiber.deletions;
  if (deletions !== null) {
    // 递归删除 先处理删除的
    for (let i = 0; i < deletions.length; i++) {
      const childToDelete = deletions[i];
      commitDeletionEffects(root, parentFiber, childToDelete);
    }
  }
  // Placement |  Update | ChildDeletion ..
  if (parentFiber.subtreeFlags & MutationMask) {
    let child = parentFiber.child;
    while (child !== null) {
      // 递归处理children
      commitMutationEffectsOnFiber(child, root, lanes);
      child = child.sibling;
    }
  }
}

function commitReconciliationEffects(finishedWork) {
  const flags = finishedWork.flags;
  // 不同的副作用执行不同的操作
  if (flags & Placement) {
    // 插入操作
    commitPlacement(finishedWork);
    finishedWork.flags &= ~Placement;
  }
}

// 处理插入的
function commitPlacement(finishedWork) {
  // 找到host的fiber 找到真实的dom节点
  const parentFiber = getHostParentFiber(finishedWork);
  switch (parentFiber.tag) {
    case HostComponent: {
      const parent = parentFiber.stateNode;
      const before = getHostSibling(finishedWork);
      insertOrAppendPlacementNode(finishedWork, before, parent);
      break;
    }
    case HostRoot: {
      // div#root
      const parent = parentFiber.stateNode.containerInfo;
      // 获取最近的弟弟dom节点
      const before = getHostSibling(finishedWork);
      // 插入或者追加一个
      insertOrAppendPlacementNodeIntoContainer(finishedWork, before, parent);
      break;
    }
    default:
      break;
  }
}

function getHostParentFiber(fiber) {
  let parent = fiber.return;
  while (parent !== null) {
    // hostComponent hostRoot
    if (isHostParent(parent)) {
      return parent;
    }
    parent = parent.return;
  }
}

function isHostParent(fiber) {
  return fiber.tag === HostComponent || fiber.tag === HostRoot;
}

/**
 * 找到插入的喵点
 * 找到可以插入在他的前面的那个fiber对应的真实dom节点
 * 1. 找弟弟 如果弟弟有不是新增的(可以作为参考点)
 * 2. 没有弟弟就找 return (parent) 找return的弟弟
 * 3. 不能是新插入的节点 在真实的dom中不存在 不能作为插入点
 * @param {*} fiber
 * @returns
 */
function getHostSibling(fiber) {
  let node = fiber;
  // 给while循环取名 siblings
  siblings: while (true) {
    while (node.sibling === null) {
      if (node.return === null || isHostParent(node.return)) {
        // 没有before的插入点
        return null;
      }
      // 没有弟弟就找return
      node = node.return;
    }
    node.sibling.return = node.return;
    // 找弟弟
    node = node.sibling;
    // 如果不是原生节点
    while (node.tag !== HostComponent && node.tag !== HostRoot) {
      // 如果是一个新的需要插入的就找弟弟 插入的不是真实存在的dom
      if (node.flags & Placement) {
        continue siblings;
      }
      if (node.child === null) {
        continue siblings;
      } else {
        node.child.return = node;
        node = node.child;
      }
    }
    // 不是插入的就表示是真实存在的节点
    if (!(node.flags & Placement)) {
      return node.stateNode;
    }
  }
}

function insertOrAppendPlacementNode(node, before, parent) {
  const { tag } = node;
  const isHost = tag === HostComponent || tag === HostText;
  if (isHost) {
    const stateNode = node.stateNode;
    if (before) {
      insertBefore(parent, stateNode, before);
    } else {
      appendChild(parent, stateNode);
    }
  } else {
    const child = node.child;
    if (child !== null) {
      insertOrAppendPlacementNode(child, before, parent);
      let sibling = child.sibling;
      while (sibling !== null) {
        insertOrAppendPlacementNode(sibling, before, parent);
        sibling = sibling.sibling;
      }
    }
  }
}

// 插入
function insertOrAppendPlacementNodeIntoContainer(node, before, parent) {
  const { tag } = node;
  const isHost = tag === HostComponent || tag === HostText;
  if (isHost) {
    const stateNode = node.stateNode;
    if (before) {
      // insertBefore
      insertInContainerBefore(parent, stateNode, before);
    } else {
      // appendChild
      appendChildToContainer(parent, stateNode);
    }
  }
  // else if(tag === HostPortal ) {
  // }
  else {
    // 组件
    const child = node.child;
    if (child !== null) {
      insertOrAppendPlacementNodeIntoContainer(child, before, parent);
      let sibling = child.sibling;
      while (sibling !== null) {
        insertOrAppendPlacementNodeIntoContainer(sibling, before, parent);
        sibling = sibling.sibling;
      }
    }
  }
}

let hostParent = null;
let hostParentIsContainer = false;

// 提交删除的副作用
function commitDeletionEffects(root, returnFiber, deletedFiber) {
  let parent = returnFiber;
  // 找到parent 要通过parent.remove()
  findParent: while (parent !== null) {
    switch (parent.tag) {
      case HostComponent: {
        hostParent = parent.stateNode;
        hostParentIsContainer = false;
        break findParent;
      }
      case HostRoot: {
        // 根节点 div#root
        hostParent = parent.stateNode.containerInfo;
        hostParentIsContainer = true;
        break findParent;
      }
      // case HostPortal: {
      //   hostParent = parent.stateNode.containerInfo;
      //   hostParentIsContainer = true;
      //   break findParent;
      // }
    }
    parent = parent.return;
  }
  commitDeletionEffectsOnFiber(root, returnFiber, deletedFiber);
  hostParent = null;
  hostParentIsContainer = false;
  detachFiberMutation(deletedFiber);
}

function detachFiberMutation(fiber) {
  const alternate = fiber.alternate;
  if (alternate !== null) {
    alternate.return = null;
  }
  fiber.return = null;
}

/**
 *
 * @param {*} finishedRoot
 * @param {*} nearestMountedAncestor 最近的完成的祖先
 * @param {*} deletedFiber
 * @returns
 */
function commitDeletionEffectsOnFiber(
  finishedRoot,
  nearestMountedAncestor,
  deletedFiber
) {
  // onCommitUnmount(deletedFiber);
  // 判断不同的tag类型 又需要递归的删除
  switch (deletedFiber.tag) {
    case HostComponent: {
    }
    case HostText: {
      const prevHostParent = hostParent;
      const prevHostParentIsContainer = hostParentIsContainer;
      hostParent = null;
      // 先递归的删除child 有必要吗? div都删除了 里面的节点要需要递归删除???
      // 向下递归删除的
      // 组件还有一些生命周期函数要处理 要递归处理的
      recursivelyTraverseDeletionEffects(
        finishedRoot,
        nearestMountedAncestor,
        deletedFiber
      );
      hostParent = prevHostParent;
      hostParentIsContainer = prevHostParentIsContainer;
      // 原生节点可以直接删除
      if (hostParent !== null) {
        // parentInstance.removeChild(child);
        if (hostParentIsContainer) {
          removeChildFromContainer(hostParent, deletedFiber.stateNode);
        } else {
          removeChild(hostParent, deletedFiber.stateNode);
        }
      }
      return;
    }
    case FunctionComponent: {
      recursivelyTraverseDeletionEffects(
        finishedRoot,
        nearestMountedAncestor,
        deletedFiber
      );
      return;
    }
    case ClassComponent: {
      recursivelyTraverseDeletionEffects(
        finishedRoot,
        nearestMountedAncestor,
        deletedFiber
      );
      return;
    }
    default: {
      recursivelyTraverseDeletionEffects(
        finishedRoot,
        nearestMountedAncestor,
        deletedFiber
      );
      return;
    }
  }
}

// 递归执行删除
function recursivelyTraverseDeletionEffects(
  finishedRoot,
  nearestMountedAncestor,
  parent
) {
  let child = parent.child;
  while (child !== null) {
    // 有儿子就递归删除
    commitDeletionEffectsOnFiber(finishedRoot, nearestMountedAncestor, child);
    child = child.sibling;
  }
}

export function commitPassiveUnmountEffects(finishedWork) {
  commitPassiveUnmountOnFiber(finishedWork);
}

// 第一次是只有挂载的
export function commitPassiveMountEffects(
  root,
  finishedWork,
  committedLanes,
  committedTransitions
) {
  commitPassiveMountOnFiber(
    root,
    finishedWork,
    committedLanes,
    committedTransitions
  );
}

function commitPassiveMountOnFiber(
  finishedRoot,
  finishedWork,
  committedLanes,
  committedTransitions
) {
  const flags = finishedWork.flags;
  switch (finishedWork.tag) {
    case FunctionComponent: {
      // 递归处理
      recursivelyTraversePassiveMountEffects(
        finishedRoot,
        finishedWork,
        committedLanes,
        committedTransitions
      );
      if (flags & Passive) {
        // 提交
        commitHookPassiveMountEffects(
          finishedWork,
          HookPassive | HookHasEffect
        );
      }
      break;
    }
    case HostRoot: {
      recursivelyTraversePassiveMountEffects(
        finishedRoot,
        finishedWork,
        committedLanes,
        committedTransitions
      );
      if (flags & Passive) {
      }
      break;
    }
    default: {
      recursivelyTraversePassiveMountEffects(
        finishedRoot,
        finishedWork,
        committedLanes,
        committedTransitions
      );
      break;
    }
  }
}

// 也是判断tag
function commitPassiveUnmountOnFiber(finishedWork) {
  switch (finishedWork.tag) {
    case FunctionComponent: {
      // 先递归处理
      recursivelyTraversePassiveUnmountEffects(finishedWork);
      if (finishedWork.flags & Passive) {
        commitHookPassiveUnmountEffects(
          finishedWork,
          finishedWork.return,
          HookPassive | HookHasEffect
        );
      }
      break;
    }
    default: {
      recursivelyTraversePassiveUnmountEffects(finishedWork);
      break;
    }
  }
}

// 递归
function recursivelyTraversePassiveMountEffects(
  root,
  parentFiber,
  committedLanes,
  committedTransitions
) {
  if (parentFiber.subtreeFlags & PassiveMask) {
    let child = parentFiber.child;
    while (child !== null) {
      commitPassiveMountOnFiber(
        root,
        child,
        committedLanes,
        committedTransitions
      );
      child = child.sibling;
    }
  }
}

// 先递归
function recursivelyTraversePassiveUnmountEffects(parentFiber) {
  // const deletions = parentFiber.deletions;
  // if ((parentFiber.flags & ChildDeletion) !== NoFlags) {
  // }

  if (parentFiber.subtreeFlags & PassiveMask) {
    let child = parentFiber.child;
    while (child !== null) {
      commitPassiveUnmountOnFiber(child);
      child = child.sibling;
    }
  }
}

// 提交
function commitHookPassiveMountEffects(finishedWork, hookFlags) {
  commitHookEffectListMount(hookFlags, finishedWork);
}

function commitHookPassiveUnmountEffects(
  finishedWork,
  nearestMountedAncestor,
  hookFlags
) {
  commitHookEffectListUnmount(hookFlags, finishedWork, nearestMountedAncestor);
}

function commitHookEffectListMount(flags, finishedWork) {
  const updateQueue = finishedWork.updateQueue;
  const lastEffect = updateQueue !== null ? updateQueue.lastEffect : null;
  if (lastEffect !== null) {
    // 循环链表
    const firstEffect = lastEffect.next;
    let effect = firstEffect;
    do {
      if ((effect.tag & flags) === flags) {
        // Mount
        const create = effect.create;
        effect.destroy = create();
      }
      effect = effect.next;
    } while (effect !== null && effect !== firstEffect);
  }
}

function commitHookEffectListUnmount(
  flags,
  finishedWork,
  nearestMountedAncestor
) {
  const updateQueue = finishedWork.updateQueue;
  const lastEffect = updateQueue !== null ? updateQueue.lastEffect : null;
  if (lastEffect !== null) {
    const firstEffect = lastEffect.next;
    let effect = firstEffect;
    do {
      if ((effect.tag & flags) === flags) {
        const destroy = effect.destroy;
        effect.destroy = undefined;
        if (destroy !== undefined) {
          destroy();
          // safelyCallDestroy(finishedWork, nearestMountedAncestor, destroy);
        }
      }
      effect = effect.next;
    } while (effect !== firstEffect);
  }
}

export function commitLayoutEffects(finishedWork, root, committedLanes) {
  const current = finishedWork.alternate;
  commitLayoutEffectOnFiber(root, current, finishedWork, committedLanes);
}

function commitLayoutEffectOnFiber(
  finishedRoot,
  current,
  finishedWork,
  committedLanes
) {
  const flags = finishedWork.flags;
  switch (finishedWork.tag) {
    case FunctionComponent:
      recursivelyTraverseLayoutEffects(
        finishedRoot,
        finishedWork,
        committedLanes
      );
      if (flags & Update) {
        commitHookLayoutEffects(finishedWork, HookLayout | HookHasEffect);
      }
      break;
    case ClassComponent: {
      recursivelyTraverseLayoutEffects(
        finishedRoot,
        finishedWork,
        committedLanes
      );
      if (flags & Update) {
        // commitClassLayoutLifecycles(finishedWork, current);
      }
      if (flags & Ref) {
        safelyAttachRef(finishedWork, finishedWork.return);
      }
      break;
    }
    case HostRoot: {
      recursivelyTraverseLayoutEffects(
        finishedRoot,
        finishedWork,
        committedLanes
      );
      // if(flags & Callback) {}

      break;
    }
    case HostComponent: {
      recursivelyTraverseLayoutEffects(
        finishedRoot,
        finishedWork,
        committedLanes
      );
      // if (current === null && flags & Update) {
      //   commitHostComponentMount(finishedWork);
      // }
      if (flags & Ref) {
        safelyAttachRef(finishedWork, finishedWork.return);
      }
      break;
    }
    default: {
      recursivelyTraverseLayoutEffects(
        finishedRoot,
        finishedWork,
        committedLanes
      );
      break;
    }
  }
}

function recursivelyTraverseLayoutEffects(root, parentFiber, lanes) {
  if (parentFiber.subtreeFlags & LayoutMask) {
    let child = parentFiber.child;
    while (child !== null) {
      const current = child.alternate;
      commitLayoutEffectOnFiber(root, current, child, lanes);
      child = child.sibling;
    }
  }
}

function commitHookLayoutEffects(finishedWork, hookFlags) {
  commitHookEffectListMount(hookFlags, finishedWork);
}

// function commitHookEffectListMount(flags, finishedWork) {
//   const updateQueue = finishedWork.updateQueue;
//   const lastEffect = updateQueue !== null ? updateQueue.lastEffect : null;
//   if (lastEffect !== null) {
//     const firstEffect = lastEffect.next;
//     let effect = firstEffect;
//     do {
//       if ((effect.tag & flags) === flags) {
//         // Mount
//         const create = effect.create;
//         effect.destroy = create();
//       }
//       effect = effect.next;
//     } while (effect !== firstEffect);
//   }
// }

// function commitHostComponentMount(finishedWork) {
//   const type = finishedWork.type;
//   const props = finishedWork.memoizedProps;
//   const instance = finishedWork.stateNode;
//   commitMount(instance, type, props, finishedWork);
// }

function safelyDetachRef(current, nearestMountedAncestor) {
  const ref = current.ref;
  const refCleanup = current.refCleanup;
  if (ref !== null) {
    if (typeof refCleanup === "function") {
    } else if (typeof ref === "function") {
    } else {
      ref.current = null;
    }
  }
}

// 处理ref
function safelyAttachRef(current, nearestMountedAncestor) {
  commitAttachRef(current);
}

function commitAttachRef(finishedWork) {
  const ref = finishedWork.ref;
  if (ref !== null) {
    const instance = finishedWork.stateNode;
    let instanceToUse;
    switch (finishedWork.tag) {
      case HostComponent:
        // instance
        instanceToUse = getPublicInstance(instance);
        break;
      default:
        instanceToUse = instance;
    }
    if (typeof ref === "function") {
      finishedWork.refCleanup = ref(instanceToUse);
    } else {
      ref.current = instanceToUse;
    }
  }
}
