import {
  getCurrentUpdatePriority,
  setCurrentUpdatePriority,
  DiscreteEventPriority,
} from "./ReactEventPriorities";
import { scheduleCallback, ImmediatePriority } from "./Scheduler";

let syncQueue = null;
let includesLegacySyncCallbacks = false;
let isFlushingSyncQueue = false;

export function scheduleLegacySyncCallback(callback) {
  includesLegacySyncCallbacks = true;
  scheduleSyncCallback(callback);
}

// 添加到同步队列中
export function scheduleSyncCallback(callback) {
  if (syncQueue === null) {
    syncQueue = [callback];
  } else {
    syncQueue.push(callback);
  }
}

// 刷新 清空同步队列
export function flushSyncCallbacks() {
  if (!isFlushingSyncQueue && syncQueue !== null) {
    isFlushingSyncQueue = true;
    let i = 0;
    const previousUpdatePriority = getCurrentUpdatePriority();
    try {
      const isSync = true;
      const queue = syncQueue;
      // SyncLane DiscreteEventPriority = 2
      setCurrentUpdatePriority(DiscreteEventPriority);
      for (; i < queue.length; i++) {
        let callback = queue[i];
        do {
          callback = callback(isSync);
        } while (callback !== null);
      }
      // 清空任务队列
      syncQueue = null;
    } catch (error) {
      console.log(error);
      if (syncQueue !== null) {
        syncQueue = syncQueue.slice(i + 1);
      }
      // 调度
      scheduleCallback(ImmediatePriority, flushSyncCallbacks);
      throw error;
    } finally {
      setCurrentUpdatePriority(previousUpdatePriority);
      isFlushingSyncQueue = false;
    }
  }
  return null;
}

export function flushSyncCallbacksOnlyInLegacyMode() {
  if (includesLegacySyncCallbacks) {
    flushSyncCallbacks();
  }
}
