// fiber的副作用标识

export const NoFlags = 0b000000000000000000000000000; // 0
export const PerformedWork = 0b000000000000000000000000001; // 1
export const Placement = 0b000000000000000000000000010; // 插入 2
export const Update = 0b000000000000000000000000100; // 更新 4
export const ChildDeletion = 0b000000000000000000000010000; // 要删除的
// effect 如果函数组件使用了 effect会有一个 1024 的flags
export const Passive = 0b000000000000000100000000000;
// ref 引用
export const Ref = 0b000000000000000001000000000;

export const Hydrating = 0b000000000000001000000000000;
export const ContentReset = 0b000000000000000000000100000;
export const Visibility = 0b000000000000010000000000000;
export const Snapshot = 0b000000000000000010000000000;

export const LayoutStatic = 0b000010000000000000000000000;
export const PassiveStatic = 0b000100000000000000000000000;

export const MountPassiveDev = 0b100000000000000000000000000;

export const BeforeMutationMask = Update | Snapshot | ChildDeletion;

export const MutationMask =
  Placement |
  Update |
  ChildDeletion |
  ContentReset |
  Ref |
  Hydrating |
  Visibility;

export const LayoutMask = Update | Ref | Visibility;

export const PassiveMask = Passive | Visibility | ChildDeletion;
