import {
  NoLane,
  SyncLane,
  DefaultLane,
  InputContinuousLane,
  IdleLane,
  getHighestPriorityLane,
  includesNonIdleWork,
} from "./ReactFiberLane";

// 当前的更新级
let currentUpdatePriority = NoLane;
export const DiscreteEventPriority = SyncLane; // 离散
export const ContinuousEventPriority = InputContinuousLane; // 连续
export const DefaultEventPriority = DefaultLane; // 默认事件优先级
export const IdleEventPriority = IdleLane; // 空闲

export function lanesToEventPriority(lanes) {
  // 获取最高优先级的lane return lanes & -lanes;
  const lane = getHighestPriorityLane(lanes);
  // lane越小优先级越高
  if (!isHigherEventPriority(DiscreteEventPriority, lane)) {
    return DiscreteEventPriority;
  }
  if (!isHigherEventPriority(ContinuousEventPriority, lane)) {
    return ContinuousEventPriority;
  }
  if (includesNonIdleWork(lane)) {
    return DefaultEventPriority;
  }
  return IdleEventPriority;
}

export function isHigherEventPriority(a, b) {
  return a !== 0 && a < b;
}

// 获取优先级
export function getCurrentUpdatePriority() {
  // currentUpdatePriority 公共变量
  return currentUpdatePriority;
}

// 设置新的优先级
export function setCurrentUpdatePriority(newPriority) {
  currentUpdatePriority = newPriority;
}

export function lowerEventPriority(a, b) {
  return a === 0 || a > b ? a : b;
}
