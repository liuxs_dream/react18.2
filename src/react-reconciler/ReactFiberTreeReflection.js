import { get as getInstance } from "../shared/ReactInstanceMap";
import { NoFlags, Placement } from "./ReactFiberFlags";
import { HostRoot } from "./ReactWorkTags";

export function isMounted(component) {
  const fiber = getInstance(component);
  if (!fiber) return false;
  return getNearestMountedFiber(fiber) === fiber;
}

export function getNearestMountedFiber(fiber) {
  let node = fiber;
  let nearestMounted = fiber;
  if (!fiber.alternate) {
    let nextNode = node;
    do {
      node = nextNode;
      if ((node.flags & Placement) !== NoFlags) {
        nearestMounted = node.return;
      }
      nextNode = node.return;
    } while (nextNode);
  } else {
    while (node.return) {
      node = node.return;
    }
  }
  if (node.tag === HostRoot) {
    return nearestMounted;
  }
  return null;
}
