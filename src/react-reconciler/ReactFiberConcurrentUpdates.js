import { NoLanes, mergeLanes, NoLane } from "./ReactFiberLane";
import { HostRoot } from "./ReactWorkTags";

const concurrentQueues = [];
let concurrentQueuesIndex = 0;
let concurrentlyUpdatedLanes = NoLanes;

/**
 *
 * @param {*} fiber 入队的fiber
 * @param {*} queue 待生效的队列
 * @param {*} update 更新
 * @param {*} lane 优先级
 * @returns
 */
export function enqueueConcurrentClassUpdate(fiber, queue, update, lane) {
  const concurrentQueue = queue;
  const concurrentUpdate = update;
  // 将更新一组组的放在 concurrentQueues 数组中
  enqueueUpdate(fiber, concurrentQueue, concurrentUpdate, lane);
  // 返回root节点
  return getRootForUpdatedFiber(fiber);
}

export function enqueueConcurrentHookUpdate(fiber, queue, update, lane) {
  const concurrentQueue = queue;
  const concurrentUpdate = update;
  enqueueUpdate(fiber, concurrentQueue, concurrentUpdate, lane);
  return getRootForUpdatedFiber(fiber);
}

function enqueueUpdate(fiber, queue, update, lane) {
  // 一批一批的
  concurrentQueues[concurrentQueuesIndex++] = fiber;
  concurrentQueues[concurrentQueuesIndex++] = queue;
  concurrentQueues[concurrentQueuesIndex++] = update;
  concurrentQueues[concurrentQueuesIndex++] = lane;
  // a | b
  concurrentlyUpdatedLanes = mergeLanes(concurrentlyUpdatedLanes, lane);
  // return a | b; 要合并赛道
  fiber.lanes = mergeLanes(fiber.lanes, lane);
  const alternate = fiber.alternate;
  if (alternate !== null) {
    alternate.lanes = mergeLanes(alternate.lanes, lane);
  }
}

/**
 * 在 prepareFreshStack 中会执行
 * 构建成更新的循环链表
 * enqueueUpdate是一批批的放的 取的时候就是一批取出来的
 * 将更新放在跟新队列中
 * 不同的对象有不同的跟新队列 不同的更新队列有不同的更新对象
 * 根节点 {payload: element: div}
 * 函数组件 msState 执行hook的链表头 hook.updateQueue 更新队列里面有很多个update {action: 更新函数}
 */
export function finishQueueingConcurrentUpdates() {
  const endIndex = concurrentQueuesIndex;
  concurrentQueuesIndex = 0;
  concurrentlyUpdatedLanes = NoLanes;
  let i = 0;
  while (i < endIndex) {
    const fiber = concurrentQueues[i];
    concurrentQueues[i++] = null;
    const queue = concurrentQueues[i];
    concurrentQueues[i++] = null;
    const update = concurrentQueues[i];
    concurrentQueues[i++] = null;
    const lane = concurrentQueues[i];
    concurrentQueues[i++] = null;
    // 构建更新的循环链表
    if (queue !== null && update !== null) {
      const pending = queue.pending;
      if (pending === null) {
        update.next = update;
      } else {
        update.next = pending.next;
        pending.next = update;
      }
      queue.pending = update;
    }

    if (lane !== NoLane) {
      markUpdateLaneFromFiberToRoot(fiber, update, lane);
    }
  }
}

export function unsafe_markUpdateLaneFromFiberToRoot(sourceFiber, lane) {
  // 找到root根节点
  const root = getRootForUpdatedFiber(sourceFiber);
  markUpdateLaneFromFiberToRoot(sourceFiber, null, lane);
  return root;
}

/**
 * 处理更新级
 * 每个更新都对应一个更新级的
 * @param {*} sourceFiber
 * @param {*} update
 * @param {*} lane
 */
function markUpdateLaneFromFiberToRoot(sourceFiber, update, lane) {
  // 更新fiber的lanes
  sourceFiber.lanes = mergeLanes(sourceFiber.lanes, lane);
  let alternate = sourceFiber.alternate;
  if (alternate !== null) {
    alternate.lanes = mergeLanes(alternate.lanes, lane);
  }
  let parent = sourceFiber.return;
  let node = sourceFiber;
  while (parent !== null) {
    parent.childLanes = mergeLanes(parent.childLanes, lane);
    alternate = parent.alternate;
    node = parent;
    parent = parent.return;
  }
}

// 向上找到root节点
function getRootForUpdatedFiber(sourceFiber) {
  let node = sourceFiber;
  let parent = node.return;
  while (parent !== null) {
    node = parent;
    parent = node.return;
  }
  return node.tag === HostRoot ? node.stateNode : null;
}
