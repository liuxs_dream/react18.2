import { createFiberRoot } from "./ReactFiberRoot";
import {
  initializeUpdateQueue,
  enqueueUpdate,
  createUpdate,
} from "./ReactFiberClassUpdateQueue";

import {
  NoLane,
  NoLanes,
  NoTimestamp,
  SyncLane,
  createLaneMap,
} from "./ReactFiberLane";

import {
  flushSync,
  requestEventTime,
  requestUpdateLane,
  scheduleUpdateOnFiber,
  batchedUpdates,
} from "./ReactFiberWorkLoop";
import { HostComponent } from "./ReactWorkTags";
import { getPublicInstance } from "../react-dom-bindings/client/ReactDOMHostConfig";

export function getPublicRootInstance(container) {
  const containerFiber = container.current;
  if (!containerFiber.child) {
    return null;
  }
  switch (containerFiber.child.tag) {
    case HostComponent:
      // return instance;
      return getPublicInstance(containerFiber.child.stateNode);
    default:
      return containerFiber.child.stateNode;
  }
}

/**
 * 创建容器
 * ReactDOM.render()在legacyCreateRootFromDOMContainer也会创建 const root = createContainer()
 * createRoot()会调用createContainer
 * @param {*} containerInfo 容器信息
 * @param {*} tag createRoot是 ConcurrentRoot render是传统的模式 LegacyRoot
 * @returns
 */
export function createContainer(
  containerInfo,
  tag,
  isStrictMode,
  concurrentUpdatesByDefaultOverride
) {
  const initialChildren = null;
  return createFiberRoot(
    containerInfo,
    tag,
    initialChildren,
    isStrictMode,
    concurrentUpdatesByDefaultOverride
  );
}

/**
 * 更新容器 将虚拟dom变成真实dom插入到容器中
 * createContainer() 得到root 然后调用 updateContainer()
 * @param {*} element
 * @param {*} container DiverRootNode containerInfo:div#root
 * @param {*} parentComponent
 * @param {*} callback
 */
export function updateContainer(element, container, parentComponent, callback) {
  const current = container.current; // rootFiber
  // performance.now()
  const eventTime = requestEventTime();
  // 1. 计算当前的lane赛道 根据mode模型判断
  // const lane = SyncLane;
  // 同步渲染返回的lane是 SyncLane
  // 并发渲染 const updateLane = getCurrentEventPriority() 默认事件的优先级
  // 确定更新的车道 获取当前的更新优先级
  const lane = requestUpdateLane(current);
  // const context = getContextForSubtree(parentComponent);
  // container.context = context // {}
  // 2. 创建更新 return {payload: null, next: null}
  const update = createUpdate(eventTime, lane);
  update.payload = { element };
  // update.callback = callback;
  // 3. 将更新入队 按照批次加入到 concurrentQueues 数组中 更新lanes
  // old 构建循环链表 加入到 concurrentQueues 数组中
  // return node.stateNode
  // 使用并发的更新 enqueueConcurrentClassUpdate 添加到concurrentQueues 数组中
  const root = enqueueUpdate(current, update, lane);
  // 4. 开始调度 从根节点开始
  scheduleUpdateOnFiber(root, current, lane, eventTime);
  return lane;
}

export { flushSync, batchedUpdates };
