import { REACT_ELEMENT_TYPE } from "../shared/ReactSymbols";
import isArray from "../shared/isArray";
import {
  createFiberFromText,
  createFiberFromElement,
  createWorkInProgress,
} from "./ReactFiber";
import { ChildDeletion, Placement } from "./ReactFiberFlags";
import { HostText } from "./ReactWorkTags";

// shouldTrackSideEffects 是否跟踪副作用
function createChildReconciler(shouldTrackSideEffects) {
  // 更新文本内容
  function updateTextNode(returnFiber, current, textContent, lanes) {
    if (current === null || current.tag !== HostText) {
      //  插入
      const created = createFiberFromText(textContent, returnFiber.mode, lanes);
      created.return = returnFiber;
      return created;
    } else {
      // 更新
      const existing = useFiber(current, textContent);
      existing.return = returnFiber;
      return existing;
    }
  }
  // 更新元素
  function updateElement(returnFiber, current, element, lanes) {
    const elementType = element.type;
    if (current !== null) {
      if (current.elementType === elementType) {
        // 都一样就可以复用 使用老的fiber
        const existing = useFiber(current, element.props);
        // existing.ref = coerceRef(returnFiber, current, element);
        existing.ref = element.ref;
        existing.return = returnFiber;
        return existing;
      }
    }
    // 新增插入的
    const created = createFiberFromElement(element, returnFiber.mode, lanes);
    // created.ref = coerceRef(returnFiber, current, element);
    created.ref = element.ref;
    created.return = returnFiber;
    return created;
  }

  // 单个的文本节点
  function reconcileSingleTextNode(
    returnFiber,
    currentFirstChild,
    textContent,
    lanes
  ) {
    if (currentFirstChild !== null && currentFirstChild.tag === HostText) {
      deleteRemainingChildren(returnFiber, currentFirstChild.sibling);
      const existing = useFiber(currentFirstChild, textContent);
      existing.return = returnFiber;
      return existing;
    }
    // 创建一个文本的 fiber节点
    const created = createFiberFromText(textContent, returnFiber.mode, lanes);
    created.return = returnFiber;
    return created;
  }
  // 放置单个的child
  function placeSingleChild(newFiber) {
    if (shouldTrackSideEffects && newFiber.alternate === null) {
      // 给fiber增加新增的flags
      newFiber.flags |= Placement;
    }
    return newFiber;
  }

  /**
   * 单个元素 协调单虚拟dom节点
   * 单节点的
   * // 1. 判断是否有老的fiber节点
   * // 2. 判断key
   * // 3. 判断type
   * @param {*} returnFiber 根fiber
   * @param {*} currentFirstChild 当前第一个 functionComponent对应的fiber
   * @param {*} element 新的虚拟dom对象
   * @param {*} lanes
   * @returns 返回第一个子fiber
   */
  function reconcileSingleElement(
    returnFiber,
    currentFirstChild,
    element,
    lanes
  ) {
    // 初次渲染child是没有的
    let child = currentFirstChild;
    const key = element.key;
    // 1. 判断是否有老的fiber
    while (child !== null) {
      // 2. 判断key 唯一标识 没有加就是null
      if (child.key === key) {
        const elementType = element.type;
        // 3. 判断type div
        if (child.elementType === elementType) {
          // 删除其他的老节点
          deleteRemainingChildren(returnFiber, child.sibling);
          // 复用fiber对象 key和type都一样就可以复用
          const existing = useFiber(child, element.props);
          // element.ref
          // existing.ref = coerceRef(returnFiber, child, element);
          existing.ref = element.ref;
          existing.return = returnFiber; // 构建fiber关系
          return existing;
        }
        // key相同 type不一样
        deleteRemainingChildren(returnFiber, child);
        break;
      } else {
        // key不一样就删掉
        deleteChild(returnFiber, child);
      }
      child = child.sibling;
    }
    // 创建一个fiber
    const created = createFiberFromElement(element, returnFiber.mode, lanes);
    created.ref = element.ref;
    // created.ref = coerceRef(returnFiber, currentFirstChild, element);
    created.return = returnFiber;
    return created;
  }

  // 删掉剩余的
  function deleteRemainingChildren(returnFiber, currentFirstChild) {
    if (!shouldTrackSideEffects) return null;
    let childToDelete = currentFirstChild;
    while (childToDelete !== null) {
      deleteChild(returnFiber, childToDelete);
      childToDelete = childToDelete.sibling;
    }
    return null;
  }

  function useFiber(fiber, pendingProps) {
    const clone = createWorkInProgress(fiber, pendingProps);
    clone.index = 0; // place的过程会给index重新赋值 在parent中排在第几个的
    clone.sibling = null;
    return clone;
  }

  function deleteChild(returnFiber, childToDelete) {
    if (!shouldTrackSideEffects) return;
    // 每次更新都是先删除的 react18改动的 以前是在effectList上的
    const deletions = returnFiber.deletions;
    if (deletions === null) {
      returnFiber.deletions = [childToDelete];
      returnFiber.flags |= ChildDeletion;
    } else {
      deletions.push(childToDelete);
    }
  }

  /**
   * reconcileChildrenArray
   * 多节点的dom-diff
   * @param {*} returnFiber
   * @param {*} currentFirstChild
   * @param {*} newChildren
   * @param {*} lanes
   * @returns
   */
  function reconcileChildrenArray(
    returnFiber,
    currentFirstChild,
    newChildren,
    lanes
  ) {
    // 返回的第一个新儿子
    let resultingFirstChild = null;
    // 上一个的新儿子
    let previousNewFiber = null;
    // 新的fiber的index
    let newIdx = 0;
    // 老的第一个child
    let oldFiber = currentFirstChild;
    // 下一个 fiber
    let nextOldFiber = null;
    let lastPlacedIndex = 0; // 暂时没用 dom-diff使用的
    // 1. 第一轮循环
    for (; oldFiber !== null && newIdx < newChildren.length; newIdx++) {
      // 暂存下一个老fiber
      if (oldFiber.index > newIdx) {
        nextOldFiber = oldFiber;
        oldFiber = null;
      } else {
        nextOldFiber = oldFiber.sibling;
      }
      // 试图更新 更新每个child 文本 element元素
      // key一样就可以更新
      const newFiber = updateSlot(
        returnFiber,
        oldFiber,
        newChildren[newIdx],
        lanes
      );
      // 找不到就返回了null
      if (newFiber === null) {
        if (oldFiber === null) {
          oldFiber = nextOldFiber;
        }
        // 直接跳出循环
        break;
      }
      // 要跟踪副作用
      if (shouldTrackSideEffects) {
        // 没有成功复用老的fiber 就删除老的 提交阶段会删除真实的dom节点
        if (oldFiber && newFiber.alternate === null) {
          deleteChild(returnFiber, oldFiber);
        }
      }
      // 放置child 指定新的fiber的位置
      lastPlacedIndex = placeChild(newFiber, lastPlacedIndex, newIdx);
      // 构建fiber链表关系
      if (previousNewFiber === null) {
        resultingFirstChild = newFiber;
      } else {
        previousNewFiber.sibling = newFiber;
      }
      previousNewFiber = newFiber;
      // 往后面移动
      oldFiber = nextOldFiber;
    }
    // 3. 如果老的有多个就删除
    if (newIdx === newChildren.length) {
      // 新的完了就删掉多余的
      deleteRemainingChildren(returnFiber, oldFiber);
      return resultingFirstChild;
    }

    // 初次渲染 2. 如果新的有多个就新增插入 这里可以处理
    if (oldFiber === null) {
      for (; newIdx < newChildren.length; newIdx++) {
        // 创建child
        const newFiber = createChild(returnFiber, newChildren[newIdx], lanes);
        if (newFiber === null) {
          continue;
        }
        // 放置child
        placeChild(newFiber, lastPlacedIndex, newIdx);
        if (previousNewFiber === null) {
          // 大儿子
          resultingFirstChild = newFiber;
        } else {
          // 弟弟 构建fiber的链条
          previousNewFiber.sibling = newFiber;
        }
        // 父fiber
        previousNewFiber = newFiber;
      }
    }

    // 4. 都没有完成 处理移动的问题
    // 4.1 将老的维护成一个map
    const existingChildren = mapRemainingChildren(returnFiber, oldFiber);
    // 遍历虚拟dom节点 在老的map中找能够复用的
    for (; newIdx < newChildren.length; newIdx++) {
      // 在map中找可以复用的节点
      const newFiber = updateFromMap(
        existingChildren,
        returnFiber,
        newIdx,
        newChildren[newIdx],
        lanes
      );
      if (newFiber !== null) {
        if (shouldTrackSideEffects) {
          if (newFiber.alternate !== null) {
            // 找到了能复用的就将map中的删掉
            existingChildren.delete(
              newFiber.key === null ? newIdx : newFiber.key
            );
          }
        }
        // 放置
        lastPlacedIndex = placeChild(newFiber, lastPlacedIndex, newIdx);
        // 维护链表关系
        if (previousNewFiber === null) {
          resultingFirstChild = newFiber;
        } else {
          previousNewFiber.sibling = newFiber;
        }
        previousNewFiber = newFiber;
      }
    }
    // 将剩下的老的删除
    if (shouldTrackSideEffects) {
      existingChildren.forEach((child) => deleteChild(returnFiber, child));
    }
    return resultingFirstChild;
  }

  // 老的fiber维护成一个map对象
  function mapRemainingChildren(returnFiber, currentFirstChild) {
    const existingChildren = new Map();
    let existingChild = currentFirstChild;
    while (existingChild !== null) {
      if (existingChild.key !== null) {
        existingChildren.set(existingChild.key, existingChild);
      } else {
        existingChildren.set(existingChild.index, existingChild);
      }
      existingChild = existingChild.sibling;
    }
    return existingChildren;
  }

  // 虚拟dom节点在老的map中找能够复用的节点
  function updateFromMap(
    existingChildren,
    returnFiber,
    newIdx,
    newChild,
    lanes
  ) {
    // 文本的情况
    if (
      (typeof newChild === "string" && newChild !== "") ||
      typeof newChild === "number"
    ) {
      const matchedFiber = existingChildren.get(newIdx) || null;
      return updateTextNode(returnFiber, matchedFiber, "" + newChild, lanes);
    }
    // 对象
    if (typeof newChild === "object" && newChild !== null) {
      switch (newChild.$$typeof) {
        case REACT_ELEMENT_TYPE: {
          const matchedFiber =
            existingChildren.get(
              newChild.key === null ? newIdx : newChild.key
            ) || null;
          return updateElement(returnFiber, matchedFiber, newChild, lanes);
        }
        default:
          return null;
      }
      // if (isArray(newChild) || getIteratorFn(newChild)) {
      //   const matchedFiber = existingChildren.get(newIdx) || null;
      //   return updateFragment(returnFiber, matchedFiber, newChild, lanes, null);
      // }
    }
    return null;
  }

  function updateSlot(returnFiber, oldFiber, newChild, lanes) {
    // 老fiber的key
    const key = oldFiber !== null ? oldFiber.key : null;
    if (typeof newChild === "string" || typeof newChild === "number") {
      if (key !== null) return null;
      // 更新文本
      return updateTextNode(returnFiber, oldFiber, "" + newChild, lanes);
    }
    if (typeof newChild === "object" && newChild !== null) {
      switch (newChild.$$typeof) {
        case REACT_ELEMENT_TYPE: {
          if (newChild.key === key) {
            // key一样就可以复用 更新元素
            return updateElement(returnFiber, oldFiber, newChild, lanes);
          } else {
            return null;
          }
        }
        default:
          return null;
      }
      // if (isArray(newChild)) {
      //   if (key !== null) return null;
      //   // return updateFragment(returnFiber, oldFiber, newChild, lanes, null);
      // }
    }
    return null;
  }

  // 创建child
  function createChild(returnFiber, newChild, lanes) {
    // 1. 如果是string
    if (
      (typeof newChild === "string" && newChild !== "") ||
      typeof newChild === "number"
    ) {
      // 创建文本节点fiber返回
      const created = createFiberFromText(
        "" + newChild,
        returnFiber.mode,
        lanes
      );
      created.return = returnFiber;
      return created;
    }
    // 2. 对象
    if (typeof newChild === "object" && newChild !== null) {
      switch (newChild.$$typeof) {
        case REACT_ELEMENT_TYPE: {
          const created = createFiberFromElement(
            newChild,
            returnFiber.mode,
            lanes
          );
          created.ref = newChild.ref;
          created.return = returnFiber;
          return created;
        }
        default:
          break;
      }
      // 如果是数组 这个case暂时没有
      if (isArray(newChild)) {
      }
    }
  }

  // 放置单个child
  function placeChild(newFiber, lastPlacedIndex, newIndex) {
    // 新的fiber的位置
    newFiber.index = newIndex;
    const current = newFiber.alternate;
    if (!shouldTrackSideEffects) {
      return lastPlacedIndex;
    }
    if (current !== null) {
      // 更新
      const oldIndex = current.index;
      if (oldIndex < lastPlacedIndex) {
        // 移动
        newFiber.flags |= Placement;
        return lastPlacedIndex;
      } else {
        return oldIndex;
      }
    } else {
      // 增加标识 表示需要创建真实的dom插入到容器中
      // 如果父fiber是初次挂载 不需要添加flags
      // 在完成阶段会将所有的节点全部添加到自己的身上
      newFiber.flags |= Placement;
      return lastPlacedIndex;
    }
  }

  /**
   * returnFiber
   * @param {*} returnFiber 新的父fiber
   * @param {*} currentFirstChild  老的第一个fiber
   * @param {*} newChild 新的虚拟dom
   * @param {*} lanes
   * @returns
   */
  function reconcileChildFibers(
    returnFiber,
    currentFirstChild,
    newChild,
    lanes
  ) {
    // 2. 对象 element: {} 虚拟dom节点是一个对象
    if (typeof newChild === "object" && newChild !== null) {
      // element: {$$typeof: REACT_ELEMENT_TYPE}
      switch (newChild.$$typeof) {
        case REACT_ELEMENT_TYPE: {
          // react元素
          return placeSingleChild(
            reconcileSingleElement(
              returnFiber,
              currentFirstChild,
              newChild,
              lanes
            )
          );
        }
      }
      // 3. 数组 root返回的是一个数组 ["hello", <span></span>]
      if (isArray(newChild)) {
        return reconcileChildrenArray(
          returnFiber,
          currentFirstChild,
          newChild,
          lanes
        );
      }
    }
    // 1. 渲染字符串
    if (typeof newChild === "string" || typeof newChild === "number") {
      return placeSingleChild(
        reconcileSingleTextNode(
          returnFiber,
          currentFirstChild,
          "" + newChild,
          lanes
        )
      );
    }
    // 4. 文本子节点
    // return null;
    return deleteRemainingChildren(returnFiber, currentFirstChild);
  }
  return reconcileChildFibers;
}

export const reconcileChildFibers = createChildReconciler(true);
export const mountChildFibers = createChildReconciler(false);

// This  FiberNode doesn't have work, but its subtree does. Clone the child
// fibers and continue.
export function cloneChildFibers(current, workInProgress) {
  if (workInProgress.child === null) return;
  let currentChild = workInProgress.child;
  // 创建正在工作的fiber
  let newChild = createWorkInProgress(currentChild, currentChild.pendingProps);
  workInProgress.child = newChild;

  newChild.return = workInProgress;
  while (currentChild.sibling !== null) {
    currentChild = currentChild.sibling;
    newChild = newChild.sibling = createWorkInProgress(
      currentChild,
      currentChild.pendingProps
    );
    newChild.return = workInProgress;
  }
  newChild.sibling = null;
}
