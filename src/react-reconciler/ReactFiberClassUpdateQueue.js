import {
  NoLanes,
  removeLanes,
  isSubsetOfLanes,
  mergeLanes,
  OffscreenLane,
} from "./ReactFiberLane";
import {
  enqueueConcurrentClassUpdate,
  unsafe_markUpdateLaneFromFiberToRoot,
} from "./ReactFiberConcurrentUpdates";
import {
  isUnsafeClassRenderPhaseUpdate,
  getWorkInProgressRootRenderLanes,
} from "./ReactFiberWorkLoop";

export const UpdateState = 0;
export const ReplaceState = 1;
export const ForceUpdate = 2;
export const CaptureUpdate = 3;

//  初始化队列
export function initializeUpdateQueue(fiber) {
  const queue = {
    baseState: fiber.memoizedState, // 跳过前的state
    firstBaseUpdate: null, // 单向链表 第一个被跳过的更新
    lastBaseUpdate: null,
    shared: {
      // shared是一个循环链表
      pending: null,
      lanes: NoLanes,
    },
    callbacks: null,
  };
  fiber.updateQueue = queue;
}

// 创建一个更新
export function createUpdate(eventTime, lane) {
  const update = {
    eventTime,
    lane, // 更新的优先级
    tag: UpdateState, // 更新的tag
    payload: null,
    next: null,
    callback: null,
  };

  return update;
}

// 更新入队
export function enqueueUpdate(fiber, update, lane) {
  // 获取更新队列
  const updateQueue = fiber.updateQueue;
  // 在 initializeUpdateQueue 的时候会给fiber的updateQueue赋值
  if (updateQueue === null) return null;
  // 获取共享队列
  const sharedQueue = updateQueue.shared;
  // updateQueue = {shared: {pending: null}}
  if (isUnsafeClassRenderPhaseUpdate(fiber)) {
    // 构建环形链表
    const pending = sharedQueue.pending;
    if (pending === null) {
      update.next = update;
    } else {
      update.next = pending.next;
      pending.next = update;
    }
    sharedQueue.pending = update;
    return unsafe_markUpdateLaneFromFiberToRoot(fiber, lane);
  } else {
    // 执行到了这里
    return enqueueConcurrentClassUpdate(fiber, sharedQueue, update, lane);
  }
}

export function cloneUpdateQueue(current, workInProgress) {
  // 新的更新队列
  const queue = workInProgress.updateQueue;
  // 老的更新队列
  const currentQueue = current.updateQueue;
  if (queue === currentQueue) {
    // 如果两个一样就克隆一份
    const clone = {
      baseState: currentQueue.baseState,
      firstBaseUpdate: currentQueue.firstBaseUpdate,
      lastBaseUpdate: currentQueue.lastBaseUpdate,
      shared: currentQueue.shared,
      callbacks: null,
    };
    workInProgress.updateQueue = clone;
  }
}

/**
 * 处理更新队列
 * 基于老状态和更新队列计算出新的状态给memoizedState
 * 合并新旧的链表 上次跳过的一些任务要做合并
 * 高优任务会执行多次
 * 中间的状态可能会混乱的 但是最终的结果一定是正确的
 * @param {*} workInProgress 要计算的fiber
 * @param {*} props
 * @param {*} instance
 * @param {*} renderLanes
 */
export function processUpdateQueue(
  workInProgress,
  props,
  instance,
  renderLanes
) {
  const queue = workInProgress.updateQueue;
  // base是单向链表的 pendingQueue才是循环链表
  // 老链表的链表头
  let firstBaseUpdate = queue.firstBaseUpdate;
  // 老的链表尾
  let lastBaseUpdate = queue.lastBaseUpdate;
  // 新的链表尾
  let pendingQueue = queue.shared.pending;
  // 有更新
  if (pendingQueue !== null) {
    // 清楚等待生效的更新
    queue.shared.pending = null;
    // 最后一个更新 新链表的尾部
    const lastPendingUpdate = pendingQueue;
    // 第一个更新 先链表的链表头
    const firstPendingUpdate = lastPendingUpdate.next;
    // 断开循环链表 变成一个单链表
    lastPendingUpdate.next = null;

    if (lastBaseUpdate === null) {
      // 没有老链表
      firstBaseUpdate = firstPendingUpdate;
    } else {
      // 有老的链表就将老的链表尾指向新的链表头 两个链表就变成了一个
      lastBaseUpdate.next = firstPendingUpdate;
    }
    // 指向新的链表尾 两个链表就合并为一个了
    lastBaseUpdate = lastPendingUpdate;
    // const current = workInProgress.alternate;
    // if (current !== null) {
    //   const currentQueue = current.updateQueue;
    //   const currentLastBaseUpdate = currentQueue.lastBaseUpdate;
    //   if (currentLastBaseUpdate !== lastBaseUpdate) {
    //     if (currentLastBaseUpdate === null) {
    //       currentQueue.firstBaseUpdate = firstPendingUpdate;
    //     } else {
    //       currentLastBaseUpdate.next = firstPendingUpdate;
    //     }
    //     currentQueue.lastBaseUpdate = lastPendingUpdate;
    //   }
    // }
  }
  // 链表不为空 有更新
  if (firstBaseUpdate !== null) {
    // 上次跳过的更新前的状态
    let newState = queue.baseState;
    // 没执行的更新lanes
    let newLanes = NoLanes;
    let newBaseState = null;
    let newFirstBaseUpdate = null;
    let newLastBaseUpdate = null;
    let update = firstBaseUpdate;
    do {
      const updateEventTime = update.eventTime;
      // 获取更新的车道
      const updateLane = update.lane;
      const shouldSkipUpdate = !isSubsetOfLanes(renderLanes, updateLane);
      // const updateLane = removeLanes(update.lane, OffscreenLane);
      // const isHiddenUpdate = updateLane !== update.lane;
      // // 是否需要跳过更新 如果优先级不够就要跳过
      // const shouldSkipUpdate = isHiddenUpdate
      //   ? // workInProgressRootRenderLanes
      //     !isSubsetOfLanes(getWorkInProgressRootRenderLanes(), updateLane)
      //   : !isSubsetOfLanes(renderLanes, updateLane);
      if (shouldSkipUpdate) {
        // 跳过低优先级的任务 跳过的更新要保存起来 下次合并了执行
        const clone = {
          eventTime: updateEventTime,
          lane: updateLane,
          tag: update.tag,
          payload: update.payload,
          callback: update.callback,
          next: null,
        };
        // 跳过的base链表为空
        if (newLastBaseUpdate === null) {
          // 都指向第一次跳过的更新
          newFirstBaseUpdate = newLastBaseUpdate = clone;
          // 跳过的时候的状态值
          newBaseState = newState;
        } else {
          newLastBaseUpdate = newLastBaseUpdate.next = clone;
        }
        // 新的赛道
        newLanes = mergeLanes(newLanes, updateLane);
      } else {
        // 表示已经有跳过的更新了 后面的都要保存下来
        if (newLastBaseUpdate !== null) {
          const clone = {
            eventTime: updateEventTime,
            lane: NoLane, // 0是任何的子集 不会被跳过了
            tag: update.tag,
            payload: update.payload,
            callback: null,
            next: null,
          };
          newLastBaseUpdate = newLastBaseUpdate.next = clone;
        }
        // 计算新状态 初次渲染 element
        // 可能是中间状态的 中间状态的state可能是错误的
        newState = getStateFromUpdate(
          workInProgress,
          queue,
          update,
          newState,
          props,
          instance
        );
        // const callback = update.callback;
        // if (callback !== null) {
        // }
      }
      update = update.next;
      if (update === null) {
        pendingQueue = queue.shared.pending;
        if (pendingQueue === null) {
          break;
        } else {
          const lastPendingUpdate = pendingQueue;
          const firstPendingUpdate = lastPendingUpdate.next;
          lastPendingUpdate.next = null;
          update = firstPendingUpdate;
          queue.lastBaseUpdate = lastPendingUpdate;
          queue.shared.pending = null;
        }
      }
    } while (true);
    // 存储新的值
    if (newLastBaseUpdate === null) {
      // 没有跳过的更新
      newBaseState = newState;
    }
    queue.baseState = newBaseState;
    queue.firstBaseUpdate = newFirstBaseUpdate;
    queue.lastBaseUpdate = newLastBaseUpdate;
    // if (firstBaseUpdate === null) {
    //   queue.shared.lanes = NoLanes;
    // }
    workInProgress.lanes = newLanes;
    // 渲染完还会判断是否还有哪些不为0的lane 如果有就会再次的渲染
    workInProgress.memoizedState = newState;
  }
}

// export function processUpdateQueue(
//   workInProgress,
//   props,
//   instance,
//   renderLanes
// ) {
//   const queue = workInProgress.updateQueue;
//   const pendingQueue = queue.shared.pending;
//   if (pendingQueue !== null) {
//     queue.shared.pending = null;
//     // 将循环链表剪开
//     const lastPendingUpdate = pendingQueue;
//     const firstPendingUpdate = lastPendingUpdate.next;
//     lastPendingUpdate.next = null;
//     let newState = workInProgress.memoizedState; // 老的状态
//     let update = firstPendingUpdate;
//     while (update) {
//       // 计算新的状态
//       newState = getStateFromUpdate(
//         workInProgress,
//         queue,
//         update,
//         newState,
//         props,
//         instance
//       );
//       update = update.next;
//     }
//     // 重新赋值
//     workInProgress.memoizedState = newState;
//   }
// }

/**
 * 计算新的状态
 * @param {*} workInProgress
 * @param {*} queue
 * @param {*} update 更新的对象其实有很多个类型
 * @param {*} prevState
 * @param {*} nextProps
 * @param {*} instance
 * @returns
 */
function getStateFromUpdate(
  workInProgress,
  queue,
  update,
  prevState,
  nextProps,
  instance
) {
  // 判断不同的更新类型
  switch (update.tag) {
    // createUpdate的时候 tag是 UpdateState
    case UpdateState: {
      const payload = update.payload;
      let partialState;
      // 可能是函数
      if (typeof payload === "function") {
        partialState = payload.call(instance, prevState, nextProps);
      } else {
        partialState = payload;
      }
      if (partialState === null || partialState === undefined) {
        return prevState;
      }
      return Object.assign({}, prevState, partialState);
    }
  }
  return prevState;
}
