// hooks相关的逻辑

import ReactSharedInternals from "../shared/ReactSharedInternals";
import is from "../shared/objectIs";
import {
  enqueueConcurrentClassUpdate,
  enqueueConcurrentHookUpdate,
} from "./ReactFiberConcurrentUpdates";
import { NoLanes, isSubsetOfLanes, mergeLanes, NoLane } from "./ReactFiberLane";
import {
  requestEventTime,
  requestUpdateLane,
  scheduleUpdateOnFiber,
} from "./ReactFiberWorkLoop";
import {
  Passive as PassiveEffect,
  PassiveStatic as PassiveStaticEffect,
  Update as UpdateEffect,
} from "./ReactFiberFlags";

import {
  HasEffect as HookHasEffect,
  Layout as HookLayout,
  Passive as HookPassive,
  Insertion as HookInsertion,
} from "./ReactHookEffectTags";

import {
  readContext,
  //  checkIfContextChanged
} from "./ReactFiberNewContext";

const { ReactCurrentDispatcher } = ReactSharedInternals;

let renderLanes = NoLanes;
// 当前渲染的fiber fiber的memoizedState指向hook的单向链表
let currentlyRenderingFiber = null;
// 当前的hook 每个hook里面有memoizedState来保存hook的状态
// queue 是更新的循环链表 next指向下一个更新
let currentHook = null;
// 正在工作的hook
let workInProgressHook = null;

// 挂载的hook
const HooksDispatcherOnMount = {
  useReducer: mountReducer,
  useState: mountState,
  useEffect: mountEffect,
  useLayoutEffect: mountLayoutEffect,
  useRef: mountRef,
  useContext: readContext,
};

// 更新的时候
const HooksDispatcherOnUpdate = {
  useReducer: updateReducer,
  useState: updateState,
  useEffect: updateEffect,
  useLayoutEffect: updateLayoutEffect,
  useRef: updateRef,
  useContext: readContext,
};

// ================= useReducer =================
export function mountReducer(reducer, initialArg) {
  // hook的单向链表 fiber的memoizedState指向第一个hook
  const hook = mountWorkInProgressHook();
  // 每个hook中有一个memoizedState来维护状态
  hook.memoizedState = hook.baseState = initialArg;
  const queue = {
    pending: null,
    lanes: NoLanes,
    dispatch: null,
    lastRenderedReducer: reducer,
    lastRenderedState: initialArg,
  };
  // 每个hook有一个更新队列是一个循环链表
  hook.queue = queue;
  // 返回的dispatch函数
  const dispatch = (queue.dispatch = dispatchReducerAction.bind(
    null,
    currentlyRenderingFiber,
    queue
  ));
  // const [number, setNumber] = React.useReducer(reducer, state)
  return [hook.memoizedState, dispatch];
}

// 更新  classComponentQueue中的 processUpdateQueue 也要在这里实现
// 存在跳过的更新 需要合并两个链表的 hooks也是有跳过的逻辑的
function updateReducer(reducer) {
  const hook = updateWorkInProgressHook();
  const queue = hook.queue;
  queue.lastRenderedReducer = reducer;
  const current = currentHook;
  let baseQueue = current.baseQueue;
  const pendingQueue = queue.pending;
  if (pendingQueue !== null) {
    if (baseQueue !== null) {
      const baseFirst = baseQueue.next;
      const pendingFirst = pendingQueue.next;
      baseQueue.next = pendingFirst;
      pendingQueue.next = baseFirst;
    }
    current.baseQueue = baseQueue = pendingQueue;
    queue.pending = null;
  }
  if (baseQueue !== null) {
    // printQueue(baseQueue);
    const first = baseQueue.next;
    let newState = current.baseState;
    let newBaseState = null;
    let newBaseQueueFirst = null;
    let newBaseQueueLast = null;
    let update = first;
    do {
      const updateLane = update.lane;
      const shouldSkipUpdate = !isSubsetOfLanes(renderLanes, updateLane);
      if (shouldSkipUpdate) {
        const clone = {
          lane: updateLane,
          action: update.action,
          hasEagerState: update.hasEagerState,
          eagerState: update.eagerState,
          next: null,
        };
        if (newBaseQueueLast === null) {
          newBaseQueueFirst = newBaseQueueLast = clone;
          newBaseState = newState;
        } else {
          newBaseQueueLast = newBaseQueueLast.next = clone;
        }
        currentlyRenderingFiber.lanes = mergeLanes(
          currentlyRenderingFiber.lanes,
          updateLane
        );
      } else {
        if (newBaseQueueLast !== null) {
          const clone = {
            lane: NoLane,
            action: update.action,
            hasEagerState: update.hasEagerState,
            eagerState: update.eagerState,
            next: null,
          };
          newBaseQueueLast = newBaseQueueLast.next = clone;
        }
        if (update.hasEagerState) {
          newState = update.eagerState;
        } else {
          const action = update.action;
          newState = reducer(newState, action);
        }
      }
      update = update.next;
    } while (update !== null && update !== first);
    if (newBaseQueueLast === null) {
      newBaseState = newState;
    } else {
      newBaseQueueLast.next = newBaseQueueFirst;
    }
    hook.memoizedState = newState;
    hook.baseState = newBaseState;
    hook.baseQueue = newBaseQueueLast;
    queue.lastRenderedState = newState;
  }
  if (baseQueue === null) {
    queue.lanes = NoLanes;
  }
  const dispatch = queue.dispatch;
  return [hook.memoizedState, dispatch];
}

// export function updateReducer(reducer, initialArg, init) {
//   // 获取新的hook
//   const hook = updateWorkInProgressHook();
//   // 更新队列
//   const queue = hook.queue;
//   const pendingQueue = queue.pending;
//   // 获取老的hook
//   const current = currentHook;
//   let newState = current.memoizedState;
//   if (pendingQueue !== null) {
//     queue.pending = null;
//     // 第一个更新
//     const first = pendingQueue.next;
//     let update = first;
//     do {
//       if (update.hasEagerState) {
//         // 如果已经计算过了就不需要在计算了
//         newState = update.eagerState;
//       } else {
//         //  计算新的状态
//         const action = update.action;
//         newState = reducer(newState, action);
//       }

//       update = update.next;
//     } while (update !== null && update !== first);
//   }
//   hook.memoizedState = newState;
//   return [hook.memoizedState, queue.dispatch];
// }

// 派发函数
export function dispatchReducerAction(fiber, queue, action) {
  console.log("dispatchReducerAction", fiber, queue, action);
  const lane = requestUpdateLane(fiber);
  const update = {
    lane,
    action,
    hasEagerState: false,
    eagerState: null,
    next: null,
  };
  // 更新入队
  const root = enqueueConcurrentHookUpdate(fiber, queue, update, lane);
  if (root !== null) {
    const eventTime = requestEventTime();
    // 开始调度
    scheduleUpdateOnFiber(root, fiber, lane, eventTime);
  }
}

// ================= useState =================

/**
 * hook.memoizedState 当前hook真正显示出现的状态
 * baseState 第一个跳出的更新之前的状态
 * hook.queue.lastRenderedState 上一个计算的状态 一般是和 memoizedState 是一样的
 * @param {*} initialState
 * @returns
 */
function mountState(initialState) {
  // hook
  const hook = mountWorkInProgressHook();
  // 支持函数的
  if (typeof initialState === "function") {
    initialState = initialState();
  }
  // 属性
  hook.memoizedState = hook.baseState = initialState;
  const queue = {
    pending: null,
    dispatch: null,
    // 上一个reducer
    lastRenderedReducer: basicStateReducer,
    // 上一个state
    lastRenderedState: initialState,
  };
  hook.queue = queue;
  // 派发函数
  const dispatch = (queue.dispatch = dispatchSetState.bind(
    null,
    currentlyRenderingFiber,
    queue
  ));
  return [hook.memoizedState, dispatch];
}

function basicStateReducer(state, action) {
  return typeof action === "function" ? action(state) : action;
}

function updateState(initialState) {
  return updateReducer(basicStateReducer, initialState);
}

// 派发函数
// 如果新状态和老状态是一样的就不需要更新
function dispatchSetState(fiber, queue, action) {
  // 当前的更新优先级 点击的时候这里的优先级发生了变化 离散优先级
  const lane = requestUpdateLane(fiber);
  const update = {
    lane,
    action,
    hasEagerState: false, // 是否有急切的更新状态
    eagerState: null,
    next: null,
  };
  const alternate = fiber.alternate;
  // 为什么要加这个判断 重新计算的时候会被覆盖
  // 在计算的时候 只要有 eagerState就会直接取值
  // 第一个更新才进行这个优化
  if (
    fiber.lanes === NoLanes &&
    (alternate === null || alternate.lanes === NoLanes)
  ) {
    // 上一个reducer函数
    const lastRenderedReducer = queue.lastRenderedReducer;
    const currentState = queue.lastRenderedState;
    // 使用上次的状态和reducer 先计算
    const eagerState = lastRenderedReducer(currentState, action);
    update.hasEagerState = true;
    update.eagerState = eagerState;
    // 如果是一样的就不需要更新调度了
    if (is(eagerState, currentState)) return;
  }
  // 入队更新开始调度
  const root = enqueueConcurrentHookUpdate(fiber, queue, update, lane);
  // 调度
  if (root !== null) {
    const eventTime = requestEventTime();
    scheduleUpdateOnFiber(root, fiber, lane, eventTime);
  }
}

// ================= useEffect =================

// useEffect 在浏览器绘制之后执行
// useLayoutEffect 在浏览器绘制之前执行
function mountEffect(create, deps) {
  return mountEffectImpl(
    // 如果函数组件使用了useEffect会有一个1024的flag
    // 有1024就表示有effect要执行
    PassiveEffect,
    // 有 HasEffect Passive 和 Layout
    HookPassive,
    create,
    deps
  );
}

function updateEffect(create, deps) {
  return updateEffectImpl(PassiveEffect, HookPassive, create, deps);
}

/** 挂载
 * useEffect 和 useLayoutEffect只是执行的时机不一样
 * @param {*} fiberFlags fiber的tag
 * @param {*} hookFlags hook的tag
 * @param {*} create 创建函数
 * @param {*} deps 依赖
 */
function mountEffectImpl(fiberFlags, hookFlags, create, deps) {
  // 拿到新的hook
  const hook = mountWorkInProgressHook();
  const nextDeps = deps === undefined ? null : deps;
  // 给fiber添加flags
  currentlyRenderingFiber.flags |= fiberFlags;
  // fiber的 memoizedState 指向的是hook的单向链表
  // useState的 memoizedState 指向的是 内部的状态
  // hook的 memoizedState 指向一个 effect的循环链表
  // fiber的updateQueue的componentUpdateQueue的lastEffect指向effect的state的循环链表的最后一个effect
  // effect为什么要单独创建一个循环链表??? 遍历 effect更方便 不需要遍历单向的effect链表
  hook.memoizedState = pushEffect(
    HookHasEffect | hookFlags,
    create,
    undefined,
    nextDeps
  );
}

// 构建循环链表
function pushEffect(tag, create, destroy, deps) {
  const effect = {
    tag, // effectTag
    create,
    destroy,
    deps,
  };
  // fiber的updateQueue
  let componentUpdateQueue = currentlyRenderingFiber.updateQueue;
  // 构建循环链表
  if (componentUpdateQueue === null) {
    // 创建一个函数组件的更新队列 {lastEffect: null}
    componentUpdateQueue = createFunctionComponentUpdateQueue();
    currentlyRenderingFiber.updateQueue = componentUpdateQueue;
    componentUpdateQueue.lastEffect = effect.next = effect;
  } else {
    // 取出最后一个
    const lastEffect = componentUpdateQueue.lastEffect;
    if (lastEffect === null) {
      componentUpdateQueue.lastEffect = effect.next = effect;
    } else {
      const firstEffect = lastEffect.next;
      lastEffect.next = effect;
      effect.next = firstEffect;
      componentUpdateQueue.lastEffect = effect;
    }
  }
  return effect;
}

function createFunctionComponentUpdateQueue() {
  return {
    lastEffect: null,
    events: null,
    stores: null,
    memoCache: null,
  };
}

/**
 * 更新
 * @param {*} fiberFlags
 * @param {*} hookFlags
 * @param {*} create
 * @param {*} deps
 */
function updateEffectImpl(fiberFlags, hookFlags, create, deps) {
  const hook = updateWorkInProgressHook();
  const nextDeps = deps === undefined ? null : deps;
  let destroy = undefined;
  // 如果依赖没变就不需要执行 直接return
  if (currentHook !== null) {
    const prevEffect = currentHook.memoizedState;
    destroy = prevEffect.destroy;
    if (nextDeps !== null) {
      const prevDeps = prevEffect.deps;
      if (areHookInputsEqual(nextDeps, prevDeps)) {
        // 还是要push到里面 维持链表
        hook.memoizedState = pushEffect(hookFlags, create, destroy, nextDeps);
        return;
      }
    }
  }
  currentlyRenderingFiber.flags |= fiberFlags;
  hook.memoizedState = pushEffect(
    HookHasEffect | hookFlags,
    create,
    destroy,
    nextDeps
  );
}

function areHookInputsEqual(nextDeps, prevDeps) {
  if (prevDeps === null) return false;
  for (let i = 0; i < prevDeps.length && i < nextDeps.length; i++) {
    if (is(nextDeps[i], prevDeps[i])) {
      continue;
    }
    return false;
  }
  return true;
}
// ================= useLayoutEffect =================

function mountLayoutEffect(create, deps) {
  let fiberFlags = UpdateEffect;
  // fiberFlags |= MountLayoutDevEffect;
  // HookLayout 4
  return mountEffectImpl(fiberFlags, HookLayout, create, deps);
}

function updateLayoutEffect(create, deps) {
  return updateEffectImpl(UpdateEffect, HookLayout, create, deps);
}

// ================= useRef =================
function mountRef(initialValue) {
  const hook = mountWorkInProgressHook();
  const ref = {
    current: initialValue,
  };
  hook.memoizedState = ref;
  return ref;
}

function updateRef() {
  const hook = updateWorkInProgressHook();
  return hook.memoizedState;
}

// =============================================================

// 构建fiber链表 hook的单向链表
export function mountWorkInProgressHook() {
  const hook = {
    memoizedState: null, // 每个hook都有一个memoizedState来保存状态
    baseState: null,
    baseQueue: null,
    queue: null, // hook的更新队列 循环链表
    next: null, // 指向下一个hook 一个函数组件中可以有很多个hook 维护为一个单向的链表
  };
  // 当前的hook
  if (workInProgressHook === null) {
    // 当前fiber的memoizedState指向第一个hook
    currentlyRenderingFiber.memoizedState = workInProgressHook = hook;
  } else {
    workInProgressHook = workInProgressHook.next = hook;
  }
  return workInProgressHook;
}

// 根据老的hook链表构建新的hook链表 返回新的hook
export function updateWorkInProgressHook() {
  let nextCurrentHook;
  // 在renderWithHook后面会将变量还原为null
  // currentHook 老的hook
  if (currentHook === null) {
    // 拿到替身 就是老的fiber对象
    const current = currentlyRenderingFiber.alternate;
    if (current !== null) {
      // 替身的memoizedState指向的是老的hook链表的第一个hook
      nextCurrentHook = current.memoizedState;
    } else {
      nextCurrentHook = null;
    }
  } else {
    nextCurrentHook = currentHook.next;
  }

  let nextWorkInProgressHook;
  if (workInProgressHook === null) {
    // fiber的memoizedState指向的是hook链表的第一个
    nextWorkInProgressHook = currentlyRenderingFiber.memoizedState;
  } else {
    nextWorkInProgressHook = workInProgressHook.next;
  }
  if (nextWorkInProgressHook !== null) {
    workInProgressHook = nextWorkInProgressHook;
    nextWorkInProgressHook = workInProgressHook.next;
    currentHook = nextCurrentHook;
  } else {
    // 从当前的hook clone
    if (nextCurrentHook === null) {
      const currentFiber = currentlyRenderingFiber.alternate;
      if (currentFiber === null) {
        const newHook = {
          memoizedState: null,
          baseState: null,
          baseQueue: null,
          queue: null,
          next: null,
        };
        nextCurrentHook = newHook;
      } else {
      }
    }
    currentHook = nextCurrentHook;
    // 根据老的hook构建新的hook
    const newHook = {
      memoizedState: currentHook.memoizedState,
      baseState: currentHook.baseState,
      baseQueue: currentHook.baseQueue,
      queue: currentHook.queue,
      next: null,
    };
    // 第一个hook
    if (workInProgressHook === null) {
      currentlyRenderingFiber.memoizedState = workInProgressHook = newHook;
    } else {
      // hook的单向链表
      workInProgressHook = workInProgressHook.next = newHook;
    }
  }
  // 返回新的hook
  return workInProgressHook;
}

/**
 * 渲染函数组件
 * @param {*} current
 * @param {*} workInProgress
 * @param {*} Component
 * @param {*} props
 * @param {*} secondArg
 * @param {*} nextRenderLanes
 */
export function renderWithHooks(
  current,
  workInProgress,
  Component,
  props,
  secondArg,
  nextRenderLanes
) {
  // 当前正在渲染的车道
  renderLanes = nextRenderLanes;
  currentlyRenderingFiber = workInProgress;
  // 在之前要将状态更新队列清空 函数组件的memoizedState指向的是hook的单向链表
  workInProgress.memoizedState = null;
  // 更新队列中是 effect对应的循环链表
  workInProgress.updateQueue = null;
  workInProgress.lanes = NoLanes;
  // 给hook赋值 一个全局变量
  ReactCurrentDispatcher.current =
    // 更新阶段
    current === null || current.memoizedState === null
      ? HooksDispatcherOnMount
      : HooksDispatcherOnUpdate;
  // 执行函数组件 得到虚拟dom节点
  // 执行的时候就会遇到 React.setState() 的调用
  let children = Component(props, secondArg);
  // 做一些还原操作
  finishRenderingHooks(current, workInProgress);
  return children;
}

function finishRenderingHooks(current, workInProgress) {
  // throwInvalidHookError
  // ReactCurrentDispatcher.current = ContextOnlyDispatcher;
  renderLanes = NoLanes;
  currentHook = null;
  currentlyRenderingFiber = null;
  workInProgressHook = null;
}

function printQueue(queue) {
  const first = queue.next;
  let desc = "";
  let update = first;
  do {
    desc += "=>" + update.action.id;
    update = update.next;
  } while (update !== null && update !== first);
  desc += "=>null";
  console.log(desc);
}
