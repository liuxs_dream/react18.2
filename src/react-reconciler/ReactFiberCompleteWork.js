import {
  ClassComponent,
  FunctionComponent,
  HostComponent,
  HostRoot,
  HostText,
  ContextProvider,
} from "./ReactWorkTags";
import { getRootHostContainer, getHostContext } from "./ReactFiberHostContext";
import {
  createTextInstance,
  createInstance,
  appendInitialChild,
  finalizeInitialChildren,
  prepareUpdate,
} from "../react-dom-bindings/client/ReactDOMHostConfig";
import { NoLanes, mergeLanes } from "./ReactFiberLane";
import { NoFlags, Ref, Update } from "./ReactFiberFlags";
import { popProvider } from "./ReactFiberNewContext";

let appendAllChildren;
let updateHostContainer;
let updateHostComponent;
let updateHostText;

updateHostContainer = function (current, workInProgress) {
  // noop
};

updateHostComponent = function (current, workInProgress, type, newProps) {
  const oldProps = current.memoizedProps;
  if (oldProps === newProps) return;
  const instance = workInProgress.stateNode;
  const currentHostContext = getHostContext();
  const updatePayload = prepareUpdate(
    instance,
    type,
    oldProps,
    newProps,
    currentHostContext
  );
  workInProgress.updateQueue = updatePayload;
  // 如果有更新就标记为 更新
  if (updatePayload) {
    // workInProgress.flags |= Update;
    markUpdate(workInProgress);
  }
};

updateHostText = function (current, workInProgress, oldText, newText) {
  if (oldText !== newText) {
    markUpdate(workInProgress);
  }
};

// 把当前完成的子节点对应的真实dom挂载到自己的父parent的真实dom节点哈桑
appendAllChildren = function (parent, workInProgress) {
  let node = workInProgress.child;
  while (node !== null) {
    // appendInitialChild(parent, node.stateNode);
    // node = node.sibling;

    // 不是所有的节点都是真实的dom节点
    // 函数组件 FunctionComponent就不是
    if (node.tag === HostComponent || node.tag === HostText) {
      // parent.appendChild(child);
      appendInitialChild(parent, node.stateNode);
    } else if (node.child !== null) {
      // 不是真实的dom节点 找他的child
      // 函数组件就是一个函数 返回一个虚拟dom 没有真实的dom节点
      node.child.return = node;
      node = node.child;
      // 找到儿子继续
      continue;
    }
    if (node === workInProgress) return;
    // 弟弟没有了
    while (node.sibling === null) {
      // 到父fiber就结束了
      if (node.return === null || node.return === workInProgress) {
        return;
      }
      // 回到父节点 functionComponent
      node = node.return;
    }
    node.sibling.return = node.return;
    // 弟弟
    node = node.sibling;
  }
};

/**
 * 完成fiber
 * 如果是原生dom类型的就会创建真实的dom节点
 * @param {*} current
 * @param {*} workInProgress
 * @param {*} renderLanes
 * @returns
 */
export function completeWork(current, workInProgress, renderLanes) {
  const newProps = workInProgress.pendingProps;
  switch (workInProgress.tag) {
    case HostText: {
      // 第一个完成的文本节点
      const newText = newProps;
      if (current && workInProgress.stateNode !== null) {
        // update更新的
        const oldText = current.memoizedProps;
        updateHostText(current, workInProgress, oldText, newText);
      } else {
        // stack
        // const rootContainerInstance = getRootHostContainer();
        // const currentHostContext = getHostContext();
        // 创建真实的dom节点 document.createTextNode(text)
        workInProgress.stateNode = createTextInstance(
          newText,
          // rootContainerInstance,
          // currentHostContext,
          workInProgress
        );
      }
      // 向上冒泡副作用 以前是收集副作用链条 effectList
      // 将儿子的副作用和自己的副作用全部上交 全部冒泡到根节点 统一处理
      bubbleProperties(workInProgress);
      return null;
    }
    case HostComponent: {
      const type = workInProgress.type;
      if (current !== null && workInProgress.stateNode != null) {
        // 更新操作 workInProgress.updateQueue = updatePayload;
        // diffProperties 比对属性
        updateHostComponent(current, workInProgress, type, newProps);
        if (current.ref !== workInProgress.ref) {
          markRef(workInProgress);
        }
      } else {
        const currentHostContext = getHostContext();
        const rootContainerInstance = getRootHostContainer();
        // (1)创建真实的dom节点
        // 1. document.createElement(type)
        // 2. 处理fiber的属性不是dom的属性 updateFiberProps
        const instance = createInstance(
          type,
          newProps,
          rootContainerInstance,
          currentHostContext,
          workInProgress
        );
        // (2) 将自己的孩子全部挂到自己的身上 初次挂载的时候是不会添加flags的
        appendAllChildren(instance, workInProgress, false, false);
        workInProgress.stateNode = instance;
        // 初始化 dom 属性 children是单独处理的(但是text的需要处理)
        if (finalizeInitialChildren(instance, type, newProps)) {
          // workInProgress.flags |= Update; 标记为更新
          markUpdate(workInProgress);
        }
        if (workInProgress.ref !== null) {
          markRef(workInProgress);
        }
      }
      bubbleProperties(workInProgress);
      return null;
    }
    case FunctionComponent: {
      // 在完成阶段是已经知道了类型了的
      bubbleProperties(workInProgress);
      return null;
    }
    case ClassComponent: {
      // 类组件
      bubbleProperties(workInProgress);
      return null;
    }
    case HostRoot: {
      // 根节点完成
      // const fiberRoot = workInProgress.stateNode;
      // updateHostContainer(current, workInProgress);
      bubbleProperties(workInProgress);
      return null;
    }
    case ContextProvider:
      const context = workInProgress.type._context;
      popProvider(context, workInProgress);
      bubbleProperties(workInProgress);
      return null;
    default:
      break;
  }
}

function bubbleProperties(completedWork) {
  let newChildLanes = NoLanes;
  let subtreeFlags = NoFlags;
  // 拿到完成的fiber的儿子
  let child = completedWork.child;
  // 遍历当前fiber的所有子节点
  while (child !== null) {
    // 合并lanes
    newChildLanes = mergeLanes(
      newChildLanes,
      mergeLanes(child.lanes, child.childLanes)
    );
    // 将child的副作用合并到上面
    subtreeFlags |= child.subtreeFlags; // child的child的副作用上交
    subtreeFlags |= child.flags; // child的flag
    child = child.sibling;
  }
  // 全部收集
  completedWork.subtreeFlags |= subtreeFlags;
  completedWork.childLanes = newChildLanes;
  // return didBailout;
}

function markUpdate(workInProgress) {
  workInProgress.flags |= Update;
}

function markRef(workInProgress) {
  workInProgress.flags |= Ref;
}
