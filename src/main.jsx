// import React from "react";
// // import ReactDOM from "react-dom";
// import { createRoot } from "react-dom/client";

import React from "./react";
// import ReactDOM from "./react-dom";
import { createRoot } from "./react-dom/client";

// import ReactDOM from "react-dom/client";

// let element1 = (
//   <div className="title" style={{ color: "red" }}>
//     <span>hello</span>world
//   </div>
// );

// 实现 React.createElement()
// console.log(JSON.stringify(element1, null, 2));

// ReactDOM.render("hello world", document.getElementById("root"));

// let element = (
//   <h1>
//     hello<span style={{ color: "red" }}>world</span>
//   </h1>
// );

// class ClassComponent extends React.Component {
//   render() {
//     return (
//       <h1>
//         hello<span style={{ color: "red" }}>classComponent</span>
//       </h1>
//     );
//   }
// }
// // type: class ClassComponent
// let element = <ClassComponent />;

// function FunctionComponent() {
//   return (
//     <h1>
//       hello<span style={{ color: "red" }}>functionComponent</span>
//     </h1>
//   );
// }

// // root之后的type是一个函数
// let element = <FunctionComponent />;

// parentCapture
// spanCapture
// spanClick
//

// function FunctionComponent() {
//   const parentClick = () => {
//     console.log("parentClick");
//   };
//   const parentCapture = () => {
//     console.log("parentCapture");
//   };
//   const spanClick = () => {
//     console.log("spanClick");
//   };
//   const spanCapture = () => {
//     console.log("spanCapture");
//   };
//   return (
//     <h1 onClick={parentClick} onClickCapture={parentCapture}>
//       hello
//       <span
//         onClick={spanClick}
//         onClickCapture={spanCapture}
//         style={{ color: "pink" }}
//       >
//         eventSystem
//       </span>
//     </h1>
//   );
// }

// const element = <FunctionComponent />;

// // 副作用标识
// // flags 1 subTreeFlags 4 classComponent
// // flags 0 subTreeFlags 4 div
// // flags 4 subTreeFlags 0 p
// // 0 0 button
// class CounterComponent extends React.Component {
//   constructor(props) {
//     super(props);
//     this.state = { number: 0 };
//   }
//   handleClick = () => {
//     debugger;
//     this.setState({ number: this.state.number + 1 });
//   };
//   render() {
//     return (
//       <div>
//         <p>我自己的: {this.state.number}</p>
//         <button onClick={this.handleClick}>+</button>
//       </div>
//     );
//   }
// }
// const element = <CounterComponent />;

// hooks

// const reducer = (state, action) => {
//   if (action.type === "add") return state + 1;
//   return state;
// };
// function Counter() {
//   debugger;
//   const [number, setNumber] = React.useReducer(reducer, 1);
//   return <button onClick={() => setNumber({ type: "add" })}>{number}</button>;
// }

// function Counter() {
//   debugger;
//   const [number, setNumber] = React.useState(1);
//   const [number1, setNumber1] = React.useState(10);
//   const handleClick = () => {
//     setNumber(number + 1);
//     setNumber(number + 2);
//     setNumber1(number1 * 2);
//   };
//   return (
//     <button onClick={handleClick}>
//       {number}: {number1}
//     </button>
//   );
// }

// const element = <Counter />;

// (1) 单个dom-diff
// 1. key相同 type相同
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <div onClick={() => setNumber(number + 1)} key="title" id="title">
//       title
//     </div>
//   ) : (
//     <div onClick={() => setNumber(number + 1)} key="title" id="title2">
//       title2
//     </div>
//   );
// }
// 2.  key不一样 type相同
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <div onClick={() => setNumber(number + 1)} key="title1" id="title">
//       title
//     </div>
//   ) : (
//     <div onClick={() => setNumber(number + 1)} key="title2" id="title2">
//       title2
//     </div>
//   );
// }
// 3. key一样 type不一样
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <div onClick={() => setNumber(number + 1)} key="title" id="title">
//       title
//     </div>
//   ) : (
//     <button onClick={() => setNumber(number + 1)} key="title" id="title2">
//       title2
//     </button>
//   );
// }
// 4. 以前有多个 现在一个节点的
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A</li>
//       <li key="B" id="B">
//         B
//       </li>
//       <li key="C">C</li>
//     </ul>
//   ) : (
//     <button onClick={() => setNumber(number + 1)} key="title" id="title2">
//       title2
//     </button>
//   );
// }

// (2) 多节点的dom-diff

// 1. 多个节点的数量和key相同 部分的type不一样
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A1</li>
//       <li key="B" id="B">
//         B1
//       </li>
//       <li key="C">C1</li>
//     </ul>
//   ) : (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A2</li>
//       <p key="B" id="B">
//         B2
//       </p>
//       <li key="C">C2</li>
//     </ul>
//   );
// }

// 2. 全部相同 有新增的元素
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A1</li>
//       <li key="B" id="B">
//         B1
//       </li>
//       <li key="C">C1</li>
//     </ul>
//   ) : (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A2</li>
//       <li key="B" id="B">
//         B2
//       </li>
//       <li key="C">C2</li>
//       <li key="D">D2</li>
//     </ul>
//   );
// }

// 3. 都相同 有需要删除的老元素
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A1</li>
//       <li key="B" id="B">
//         B1
//       </li>
//       <li key="C">C1</li>
//     </ul>
//   ) : (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A2</li>
//       <li key="B" id="B">
//         B2
//       </li>
//     </ul>
//   );
// }

// 4. 数量不同 key不同 需要处理移动的情况
// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   return number === 0 ? (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A1</li>
//       <li key="B" id="B">
//         B1
//       </li>
//       <li key="C">C1</li>
//       <li key="D">D</li>
//       <li key="E">E</li>
//       <li key="F">F</li>
//     </ul>
//   ) : (
//     <ul key="container" onClick={() => setNumber(number + 1)}>
//       <li key="A">A2</li>
//       <li key="C">C2</li>
//       <li key="E">E2</li>
//       <li key="B" id="B">
//         B2
//       </li>
//       <li key="G">G2</li>
//       <li key="D">D2</li>
//     </ul>
//   );
// }

// let element = <FunctionComponent />;

// useEffect
// function Counter() {
//   console.log("counter render");
//   const [number, setNumber] = React.useState(0);
//   React.useEffect(() => {
//     console.log("useEffect1");
//     return () => {
//       console.log("destroy useEffect1");
//     };
//   });
//   React.useEffect(() => {
//     console.log("useEffect2");
//     return () => {
//       console.log("destroy useEffect2");
//     };
//   }, []);
//   React.useEffect(() => {
//     console.log("useEffect3");
//     return () => {
//       console.log("destroy useEffect3");
//     };
//   });

//   React.useLayoutEffect(() => {
//     console.log("useLayoutEffect1");
//     return () => {
//       console.log("destroy useLayoutEffect1");
//     };
//   }, []);

//   React.useLayoutEffect(() => {
//     console.log("useLayoutEffect2");
//     return () => {
//       console.log("destroy useLayoutEffect2");
//     };
//   });

//   return (
//     <div
//       onClick={() => {
//         setNumber(number + 1);
//       }}
//     >
//       {number}
//     </div>
//   );
// }
// let element = <Counter />;

// lane模型优先级
// let element = <div>hello lane</div>;

// function FunctionComponent() {
//   const [number, setNumber] = React.useState(0);
//   // React.useEffect(() => {
//   //   setNumber((number) => number + 1);
//   //   setNumber((number) => number + 1);
//   // }, []);
//   return (
//     <button
//       onClick={() => {
//         setNumber(number + 1);
//         // setTimeout(() => {
//         //   setNumber(number + 2);
//         //   setNumber(number + 3);
//         // }, 1000);
//       }}
//     >
//       {number}
//     </button>
//   );
// }

// 高优先任务打断低优先任务

// function FunctionComponent() {
//   const [numbers, setNumbers] = React.useState(new Array(10).fill("A"));
//   const divRef = React.useRef();
//   React.useEffect(() => {
//     setTimeout(() => {
//       console.log(divRef);
//       divRef.current.click();
//     }, 10);
//     setNumbers((numbers) => numbers.map((item) => item + "B"));
//   }, []);
//   return (
//     <div
//       ref={divRef}
//       onClick={() => {
//         setNumbers((numbers) => numbers.map((item) => item + "C"));
//       }}
//     >
//       {numbers.map((number, index) => (
//         <p key={index}>{number}</p>
//       ))}
//     </div>
//   );
// }

// 饥饿问题 一直被打断 标记为过期
// let counter = 0;
// let timer;
// let bCounter = 0;
// let cCounter = 0;
// function FunctionComponent() {
//   const [numbers, setNumbers] = React.useState(new Array(10).fill("A"));
//   const divRef = React.useRef();
//   const updateB = (numbers) => new Array(10).fill(numbers[0] + "B");
//   updateB.id = "updateB" + bCounter++;
//   const updateC = (numbers) => new Array(10).fill(numbers[0] + "C");
//   updateC.id = "updateC" + cCounter++;
//   React.useEffect(() => {
//     timer = setInterval(() => {
//       divRef.current.click();
//       if (counter++ === 0) {
//         setNumbers(updateB);
//       }
//       divRef.current.click();
//       if (counter++ > 10) {
//         clearInterval(timer);
//       }
//     });
//   }, []);
//   return (
//     <div ref={divRef} onClick={() => setNumbers(updateC)}>
//       {numbers.map((number, index) => (
//         <p key={index}>{number}</p>
//       ))}
//     </div>
//   );
// }
// let element = <FunctionComponent />;

// useContext
const NameContext = React.createContext("");

function Child() {
  const name = React.useContext(NameContext);
  return <button>{name}</button>;
}

function App() {
  const [name, setName] = React.useState("a");
  return (
    <div>
      <button
        onClick={() => {
          setName(name + "a");
        }}
      >
        setName
      </button>
      <NameContext.Provider value={name}>
        <Child />
      </NameContext.Provider>
    </div>
  );
}

let element = <App />;

console.log(element);
const root = createRoot(document.getElementById("root"), {
  // 可以传递参数
  unstable_concurrentUpdatesByDefault: true,
});
// root.render("hello world");

// debugger;
root.render(element);
