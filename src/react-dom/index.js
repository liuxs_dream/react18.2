import { render } from "./ReactDOMLegacy";
import { createRoot } from "./client/ReactDOM.js";

const ReactDOM = {
  createRoot,
  render,
};

export { createRoot };

export default ReactDOM;
