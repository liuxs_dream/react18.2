import { createContainer } from "../../react-reconciler/ReactFiberReconciler";
import { ConcurrentMode } from "../../react-reconciler/ReactTypeOfMode";
import { markContainerAsRoot } from "../../react-dom-bindings/client/ReactDOMComponentTree";
import { updateContainer } from "../../react-reconciler/ReactFiberReconciler";
import { listenToAllSupportedEvents } from "../../react-dom-bindings/events/DOMPluginEventSystem";
import { allowConcurrentByDefault } from "../../shared/ReactFeatureFlags";

export function createRoot(container, options) {
  let isStrictMode = false;
  let concurrentUpdatesByDefaultOverride = false;
  if (options !== null && options !== undefined) {
    if (
      allowConcurrentByDefault &&
      options.unstable_concurrentUpdatesByDefault === true
    ) {
      concurrentUpdatesByDefaultOverride = true;
    }
  }
  // 1. 创建root容器 createFiberRoot
  // 模式是不一样的 fiber上的mode属性
  // containerInfo:{div#root}
  // rootFiber.stateNode = fiberRoot
  // fiberRoot.current = rootFiber
  const root = createContainer(
    container,
    ConcurrentMode,
    isStrictMode,
    concurrentUpdatesByDefaultOverride
  );
  // 2. node[internalContainerInstanceKey] = hostRoot;
  // markContainerAsRoot(root.current, container);
  // 3. 事件系统
  const rootContainerElement = container; // container.parentNode
  listenToAllSupportedEvents(rootContainerElement);
  // 4. 返回react的根
  // this._internalRoot = internalRoot;
  // ReactDOMRoot.prototype.render = function(children) {}
  return new ReactDOMRoot(root);
}

function ReactDOMRoot(internalRoot) {
  this._internalRoot = internalRoot;
}

ReactDOMRoot.prototype.render = function (children) {
  const root = this._internalRoot; // root
  const container = root.containerInfo; // div#root
  updateContainer(children, root, null, null);
};
