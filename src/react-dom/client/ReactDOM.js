import { setBatchingImplementation } from "../../react-dom-bindings/events/ReactDOMUpdateBatching";
import { createRoot as createRootImpl } from "./ReactDOMRoot";
import { batchedUpdates } from "../../react-reconciler/ReactFiberReconciler";

setBatchingImplementation(
  batchedUpdates // ReactFiberReconciler
  // discreteUpdates,
  // flushSyncWithoutWarningIfAlreadyRendering
);

export function createRoot(container, options) {
  return createRootImpl(container, options);
}
