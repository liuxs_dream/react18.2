export const mediaEventTypes = [
  "abort",
  "canplay",
  "canplaythrough",
  "durationchange",
  "emptied",
  "encrypted",
  "ended",
  "error",
  "loadeddata",
  "loadedmetadata",
  "loadstart",
  "pause",
  "play",
  "playing",
  "progress",
  "ratechange",
  "resize",
  "seeked",
  "seeking",
  "stalled",
  "suspend",
  "timeupdate",
  "volumechange",
  "waiting",
];

export const nonDelegatedEvents = new Set([
  "cancel",
  "close",
  "invalid",
  "load",
  "scroll",
  "toggle",
  ...mediaEventTypes,
]);

import { enableCreateEventHandleAPI } from "../../shared/ReactFeatureFlags";
import { IS_CAPTURE_PHASE } from "./EventSystemFlags";
import { createEventListenerWrapperWithPriority } from "./ReactDOMEventListener";
import { batchedUpdates } from "./ReactDOMUpdateBatching";
import {
  addEventCaptureListener,
  addEventBubbleListener,
} from "./EventListener";
import getEventTarget from "./getEventTarget";
import getListener from "./getListener";
// import * as BeforeInputEventPlugin from './plugins/BeforeInputEventPlugin';
// import * as ChangeEventPlugin from './plugins/ChangeEventPlugin';
// import * as EnterLeaveEventPlugin from './plugins/EnterLeaveEventPlugin';
// import * as SelectEventPlugin from './plugins/SelectEventPlugin';
import * as SimpleEventPlugin from "./plugins/SimpleEventPlugin";

import { allNativeEvents } from "./EventRegistry";
import { HostComponent } from "../../react-reconciler/ReactWorkTags";

// 插件系统来给 allNativeEvents 赋值
SimpleEventPlugin.registerEvents();
// EnterLeaveEventPlugin.registerEvents();
// ChangeEventPlugin.registerEvents();
// SelectEventPlugin.registerEvents();
// BeforeInputEventPlugin.registerEvents();

const listeningMarker = "_reactListening" + Math.random().toString(36).slice(2);

export function listenToAllSupportedEvents(rootContainerElement) {
  if (!rootContainerElement[listeningMarker]) {
    // 保证只监听一次
    rootContainerElement[listeningMarker] = true;
    // 遍历所有的事件 new Set() 插件系统往里面添加
    allNativeEvents.forEach((domEventName) => {
      // 有一些事件是没有冒泡阶段的 cancel close scroll toggle
      if (!nonDelegatedEvents.has(domEventName)) {
        listenToNativeEvent(domEventName, false, rootContainerElement);
      }
      listenToNativeEvent(domEventName, true, rootContainerElement);
    });
  }
}

export function listenToNativeEvent(
  domEventName,
  isCapturePhaseListener,
  target
) {
  let eventSystemFlags = 0;
  if (isCapturePhaseListener) {
    eventSystemFlags |= IS_CAPTURE_PHASE; // 4
  }
  addTrappedEventListener(
    target,
    domEventName,
    eventSystemFlags,
    isCapturePhaseListener
  );
}

/**
 * 拿到监听函数去注册
 * @param {*} targetContainer
 * @param {*} domEventName
 * @param {*} eventSystemFlags
 * @param {*} isCapturePhaseListener
 */
export function addTrappedEventListener(
  targetContainer,
  domEventName,
  eventSystemFlags,
  isCapturePhaseListener
) {
  // 不同的事件是有优先级的 拿到一个监听函数
  // let listener = dispatchEventForPluginEventSystem
  let listener = createEventListenerWrapperWithPriority(
    targetContainer,
    domEventName,
    eventSystemFlags
  );
  let unsubscribeListener;
  if (isCapturePhaseListener) {
    // target.addEventListener 捕获阶段
    unsubscribeListener = addEventCaptureListener(
      targetContainer,
      domEventName,
      listener
    );
  } else {
    // target.addEventListener 冒泡阶段
    unsubscribeListener = addEventBubbleListener(
      targetContainer,
      domEventName,
      listener
    );
  }
}

/**
 *
 * @param {*} domEventName
 * @param {*} eventSystemFlags
 * @param {*} nativeEvent
 * @param {*} targetInst
 * @param {*} targetContainer
 */
export function dispatchEventForPluginEventSystem(
  domEventName,
  eventSystemFlags,
  nativeEvent,
  targetInst,
  targetContainer
) {
  let ancestorInst = targetInst;
  // 合成事件中都是批量更新
  // ReactFiberReconciler ReactFiberWorkLoop
  // executionContext |= BatchedContext;
  batchedUpdates(() =>
    dispatchEventsForPlugins(
      domEventName,
      eventSystemFlags,
      nativeEvent,
      ancestorInst,
      targetContainer
    )
  );
}

function dispatchEventsForPlugins(
  domEventName,
  eventSystemFlags,
  nativeEvent,
  targetInst,
  targetContainer
) {
  // event.target
  const nativeEventTarget = getEventTarget(nativeEvent);
  // 派发事件的数组
  const dispatchQueue = [];
  // 提取事件 给dispatchQueue赋值
  extractEvents(
    dispatchQueue,
    domEventName,
    targetInst,
    nativeEvent,
    nativeEventTarget,
    eventSystemFlags,
    targetContainer
  );

  // 处理派发事件
  processDispatchQueue(dispatchQueue, eventSystemFlags);
}

// 提取事件 给dispatchQueue赋值
function extractEvents(
  dispatchQueue,
  domEventName,
  targetInst,
  nativeEvent,
  nativeEventTarget,
  eventSystemFlags,
  targetContainer
) {
  // 插件实现
  SimpleEventPlugin.extractEvents(
    dispatchQueue,
    domEventName,
    targetInst,
    nativeEvent,
    nativeEventTarget,
    eventSystemFlags,
    targetContainer
  );
}

/**
 * 累加单个阶段的监听函数
 * 遍历
 * 找到真实dom 然后找到props.onClick
 * @param {*} targetFiber
 * @param {*} reactName
 * @param {*} nativeEventType
 * @param {*} inCapturePhase
 * @param {*} accumulateTargetOnly
 * @param {*} nativeEvent
 * @returns
 */
export function accumulateSinglePhaseListeners(
  targetFiber,
  reactName,
  nativeEventType,
  inCapturePhase,
  accumulateTargetOnly,
  nativeEvent
) {
  const captureName = reactName !== null ? reactName + "Capture" : null;
  // onClick
  const reactEventName = inCapturePhase ? captureName : reactName;
  let listeners = [];
  let instance = targetFiber;
  let lastHostComponent = null;
  while (instance !== null) {
    const { stateNode, tag } = instance;
    // 不能为组件绑定事件
    if (tag === HostComponent && stateNode !== null) {
      lastHostComponent = stateNode;
      // if(enableCreateEventHandleAPI) {}
      if (reactEventName !== null) {
        // props.click fiber.memProps
        const listener = getListener(instance, reactEventName);
        if (listener !== null) {
          listeners.push(
            createDispatchListener(instance, listener, lastHostComponent)
          );
        }
      }
    }
    instance = instance.return;
  }
  return listeners;
}

function createDispatchListener(instance, listener, currentTarget) {
  return {
    instance,
    listener,
    currentTarget,
  };
}

export function processDispatchQueue(dispatchQueue, eventSystemFlags) {
  // 是否是捕获阶段
  const inCapturePhase = (eventSystemFlags & IS_CAPTURE_PHASE) !== 0;
  for (let i = 0; i < dispatchQueue.length; i++) {
    const { event, listeners } = dispatchQueue[i];
    processDispatchQueueItemsInOrder(event, listeners, inCapturePhase);
  }
}

// 按照顺序处理事件处理函数
// 捕获和冒泡顺序是不一样的
function processDispatchQueueItemsInOrder(
  event,
  dispatchListeners,
  inCapturePhase
) {
  let previousInstance;
  if (inCapturePhase) {
    for (let i = dispatchListeners.length - 1; i >= 0; i--) {
      const { instance, currentTarget, listener } = dispatchListeners[i];
      if (instance !== previousInstance && event.isPropagationStopped()) {
        return;
      }
      // listener(event);
      executeDispatch(event, listener, currentTarget);
      previousInstance = instance;
    }
  } else {
    for (let i = 0; i < dispatchListeners.length; i++) {
      const { instance, currentTarget, listener } = dispatchListeners[i];
      if (instance !== previousInstance && event.isPropagationStopped()) {
        return;
      }
      executeDispatch(event, listener, currentTarget);
      previousInstance = instance;
    }
  }
}

// 合成事件实例的currenttarget是在不断变化的
// event nativeEventtarget 是原始的事件元是不回变化的
// currentTarget 是当前的事件元 是不断变化的 指向当前的
function executeDispatch(event, listener, currentTarget) {
  event.currentTarget = currentTarget;
  // invokeGuardedCallbackAndCatchFirstError(type, listener, undefined, event);
  listener && listener(event);
  event.currentTarget = null;
}
