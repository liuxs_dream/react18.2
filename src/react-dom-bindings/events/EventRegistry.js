export const allNativeEvents = new Set();

/**
 * 注册两个阶段的事件 捕获 冒泡
 * 在页面中触发事件的时候 会走事件处理函数
 * 要找到对应的dom元素要执行的react事件
 * @param {*} registrationName
 * @param {*} dependencies
 */
export function registerTwoPhaseEvent(registrationName, dependencies) {
  registerDirectEvent(registrationName, dependencies);
  registerDirectEvent(registrationName + "Capture", dependencies);
}

// 合成事件 react的一个事件是对应多个事件的
export const registrationNameDependencies = {};

export function registerDirectEvent(registrationName, dependencies) {
  // react事件对应的原生事件 onClick => [click]
  registrationNameDependencies[registrationName] = dependencies;
  // 给allNativeEvents添加值 click
  for (let i = 0; i < dependencies.length; i++) {
    allNativeEvents.add(dependencies[i]);
  }
}
