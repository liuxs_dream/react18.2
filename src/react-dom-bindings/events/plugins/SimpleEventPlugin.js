import {
  registerSimpleEvents,
  topLevelEventsToReactNames,
} from "../DOMEventProperties";

import { SyntheticEvent, SyntheticMouseEvent } from "../SyntheticEvent";

import { accumulateSinglePhaseListeners } from "../DOMPluginEventSystem";
import { IS_CAPTURE_PHASE } from "../EventSystemFlags";

/**
 * 提取事件 就是将要执行的毁掉函数添加到dispatchQueue中
 * @param {*} dispatchQueue 派发队列
 * @param {*} domEventName dom事件名 click
 * @param {*} targetInst 目标fiber
 * @param {*} nativeEvent 原生event
 * @param {*} nativeEventTarget 原生事件元
 * @param {*} eventSystemFlags 标识
 * @param {*} targetContainer 目标容器
 */
function extractEvents(
  dispatchQueue,
  domEventName,
  targetInst,
  nativeEvent,
  nativeEventTarget,
  eventSystemFlags,
  targetContainer
) {
  // 通过click拿到react的事件名 onClick
  const reactName = topLevelEventsToReactNames.get(domEventName);
  let SyntheticEventCtor = SyntheticEvent;
  let reactEventType = domEventName; // click
  // 不同的事件对应不同的事件对象
  switch (domEventName) {
    case "click":
    // if (nativeEvent.button === 2) {
    //   return;
    // }
    case "dblclick":
    case "mouseout":
    case "mouseover":
      SyntheticEventCtor = SyntheticMouseEvent;
      break;
    default:
      break;
  }
  // 是不是捕获阶段
  const inCapturePhase = (eventSystemFlags & IS_CAPTURE_PHASE) !== 0;
  const accumulateTargetOnly = !inCapturePhase && domEventName === "scroll";
  // 累加单个阶段的监听
  const listeners = accumulateSinglePhaseListeners(
    targetInst,
    reactName,
    nativeEvent.type,
    inCapturePhase,
    accumulateTargetOnly,
    nativeEvent
  );
  // 有监听函数
  if (listeners.length > 0) {
    // 合成事件 不同的事件对应不同的对象
    const event = new SyntheticEventCtor(
      reactName,
      reactEventType,
      null,
      nativeEvent,
      nativeEventTarget
    );
    // event不是原生的是react合成的一个事件对象
    dispatchQueue.push({ event, listeners });
  }
}

export { registerSimpleEvents as registerEvents, extractEvents };
