import {
  DefaultEventPriority,
  DiscreteEventPriority,
  getCurrentUpdatePriority,
  setCurrentUpdatePriority,
  ContinuousEventPriority,
} from "../../react-reconciler/ReactEventPriorities";
import { getNearestMountedFiber } from "../../react-reconciler/ReactFiberTreeReflection";
import {
  HostRoot,
  SuspenseComponent,
} from "../../react-reconciler/ReactWorkTags";
import { enableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay } from "../../shared/ReactFeatureFlags";
import { getClosestInstanceFromNode } from "../client/ReactDOMComponentTree";
import { dispatchEventForPluginEventSystem } from "./DOMPluginEventSystem";
import getEventTarget from "./getEventTarget";

// 不同的事件对应不同的优先级
export function getEventPriority(domEventName) {
  switch (domEventName) {
    case "click":
      return DiscreteEventPriority;
    case "drag":
      return ContinuousEventPriority;
    default:
      return DefaultEventPriority;
  }
}

export let return_targetInst = null;

export function findInstanceBlockingEvent(
  domEventName,
  eventSystemFlags,
  targetContainer,
  nativeEvent
) {
  return_targetInst = null;
  const nativeEventTarget = getEventTarget(nativeEvent);
  // 找到最近的fiber节点
  let targetInst = getClosestInstanceFromNode(nativeEventTarget);
  // if (targetInst !== null) {
  //   const nearestMounted = getNearestMountedFiber(targetInst);
  //   if (nearestMounted === null) {
  //     targetInst = null;
  //   } else {
  //     const tag = nearestMounted.tag;
  //     if (tag === SuspenseComponent) {
  //     } else if (tag === HostRoot) {
  //       const root = nearestMounted.stateNode;
  //     } else if (nearestMounted !== targetInst) {
  //       targetInst = null;
  //     }
  //   }
  // }
  return_targetInst = targetInst;
  return null;
}

/**
 *  不同的优先级
 * @param {*} targetContainer
 * @param {*} domEventName
 * @param {*} eventSystemFlags
 * @returns
 */
export function createEventListenerWrapperWithPriority(
  targetContainer,
  domEventName,
  eventSystemFlags
) {
  // 获取事件的优先级
  const eventPriority = getEventPriority(domEventName);
  let listenerWrapper;
  switch (eventPriority) {
    case DiscreteEventPriority:
      listenerWrapper = dispatchDiscreteEvent;
      break;
    case DefaultEventPriority:
    default:
      listenerWrapper = dispatchEvent;
      break;
  }
  return listenerWrapper.bind(
    null,
    domEventName,
    eventSystemFlags,
    targetContainer
  );
}

/**
 * 派发离散事件的监听函数
 * @param {*} domEventName 事件名
 * @param {*} eventSystemFlags 阶段 0是冒泡 4是捕获
 * @param {*} container 容器 div#root
 * @param {*} nativeEvent 原生的event事件对象
 */
function dispatchDiscreteEvent(
  domEventName,
  eventSystemFlags,
  container,
  nativeEvent
) {
  // 保存老的优先级
  const previousPriority = getCurrentUpdatePriority();
  try {
    // 在点击按钮的时候 设置更新的优先级 离散事件的优先级
    setCurrentUpdatePriority(DiscreteEventPriority);
    dispatchEvent(domEventName, eventSystemFlags, container, nativeEvent);
  } finally {
    setCurrentUpdatePriority(previousPriority);
  }
}

/**
 * dispatch就是委托给容器的回调函数
 * 处理事件的时候会派发事件
 * @param {*} domEventName
 * @param {*} eventSystemFlags
 * @param {*} targetContainer
 * @param {*} nativeEvent
 */
function dispatchEvent(
  domEventName,
  eventSystemFlags,
  targetContainer,
  nativeEvent
) {
  // true
  if (enableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay) {
    dispatchEventWithEnableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay(
      domEventName,
      eventSystemFlags,
      targetContainer,
      nativeEvent
    );
  } else {
    // dispatchEventOriginal(
    //   domEventName,
    //   eventSystemFlags,
    //   targetContainer,
    //   nativeEvent
    // );
  }
}

// function dispatchEventOriginal(
//   domEventName,
//   eventSystemFlags,
//   targetContainer,
//   nativeEvent
// ) {
//   const blockedOn = findInstanceBlockingEvent(
//     domEventName,
//     eventSystemFlags,
//     targetContainer,
//     nativeEvent
//   );
//   dispatchEventForPluginEventSystem(
//     domEventName,
//     eventSystemFlags,
//     nativeEvent,
//     null,
//     targetContainer
//   );
// }

function dispatchEventWithEnableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay(
  domEventName,
  eventSystemFlags,
  targetContainer,
  nativeEvent
) {
  let blockedOn = findInstanceBlockingEvent(
    domEventName,
    eventSystemFlags,
    targetContainer,
    nativeEvent
  );
  // 从真实的dom节点上获取对应的fiber节点 在 createInstance的时候会加上属性
  // return_targetInst = getClosestInstanceFromNode(nativeEventTarget);
  if (blockedOn === null) {
    // 为插件系统派发事件
    dispatchEventForPluginEventSystem(
      domEventName,
      eventSystemFlags,
      nativeEvent,
      return_targetInst,
      targetContainer
    );
    return;
  }
}
