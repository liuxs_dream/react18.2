let batchedUpdatesImpl = function (fn, bookkeeping) {
  return fn(bookkeeping);
};
let isInsideEventHandler = false;

export function batchedUpdates(fn, a, b) {
  if (isInsideEventHandler) {
    return fn(a, b);
  }
  isInsideEventHandler = true;
  try {
    return batchedUpdatesImpl(fn, a, b);
  } finally {
    isInsideEventHandler = false;
    // finishEventHandler();
  }
}

// 在react-dom/src/client中会调用这个函数
export function setBatchingImplementation(
  _batchedUpdatesImpl,
  _discreteUpdatesImpl,
  _flushSyncImpl
) {
  batchedUpdatesImpl = _batchedUpdatesImpl;
  // discreteUpdatesImpl = _discreteUpdatesImpl;
  // flushSyncImpl = _flushSyncImpl;
}
