import { registerTwoPhaseEvent } from "./EventRegistry";

// https://github.com/facebook/react/blob/main/packages/react-dom-bindings/src/events/DOMEventProperties.js
// 简单事件
const simpleEventPluginEvents = ["click"];

export function registerSimpleEvents() {
  for (let i = 0; i < simpleEventPluginEvents.length; i++) {
    const eventName = simpleEventPluginEvents[i]; // click
    const domEventName = eventName.toLowerCase(); // click
    const capitalizedEvent = eventName[0].toUpperCase() + eventName.slice(1); // Click
    // 注册两个阶段的处理函数
    registerSimpleEvent(domEventName, "on" + capitalizedEvent); // click => onClick
  }
  // 一些name不匹配的特殊case
  registerSimpleEvent("dblclick", "onDoubleClick");
}

export const topLevelEventsToReactNames = new Map();
function registerSimpleEvent(domEventName, reactName) {
  // click=>onClick做一个映射 map对象
  // 可以从fiber的属性上取到 props.click
  // 在completeWork完成结果
  // createInstance的时候会更新fiber的props
  // updateFiberProps(props) => node[internalPropsKey] = props
  // props就是 workInProgress.pendingProps 虚拟dom的props
  // 在创建fiber的时候 const pendingProps = element.props
  // 这样就可以通过node快速的拿到props属性 props.click的值
  topLevelEventsToReactNames.set(domEventName, reactName);
  // 注册两阶段的事件
  registerTwoPhaseEvent(reactName, [domEventName]);
}
