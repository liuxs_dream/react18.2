import {
  createTextNode,
  createElement,
  setInitialProperties,
  diffProperties,
  updateProperties,
} from "./ReactDOMComponent";
import { DefaultEventPriority } from "../../react-reconciler/ReactEventPriorities";
import {
  ELEMENT_NODE,
  TEXT_NODE,
  COMMENT_NODE,
  DOCUMENT_NODE,
  DOCUMENT_TYPE_NODE,
  DOCUMENT_FRAGMENT_NODE,
} from "../shared/HTMLNodeType";
import { updateFiberProps, precacheFiberNode } from "./ReactDOMComponentTree";
import { getEventPriority } from "../events/ReactDOMEventListener";

// dom相关的操作都在这个包里
export function getPublicInstance(instance) {
  return instance;
}

export function createTextInstance(
  text,
  // rootContainerInstance,
  // hostContext,
  internalInstanceHandle
) {
  // const textNode = createTextNode(text, rootContainerInstance);
  const textNode = createTextNode(text);
  return textNode;
}

export function insertBefore(parentInstance, child, beforeChild) {
  parentInstance.insertBefore(child, beforeChild);
}

export function insertInContainerBefore(container, child, beforeChild) {
  if (container.nodeType === COMMENT_NODE) {
    container.parentNode.insertBefore(child, beforeChild);
  } else {
    container.insertBefore(child, beforeChild);
  }
}

export function appendChild(parentInstance, child) {
  parentInstance.appendChild(child);
}

export function appendChildToContainer(container, child) {
  let parentNode;
  if (container.nodeType === COMMENT_NODE) {
    parentNode = container.parentNode;
    parentNode.insertBefore(child, container);
  } else {
    parentNode = container;
    parentNode.appendChild(child);
  }
}

// 获取当前的事件优先级
export function getCurrentEventPriority() {
  const currentEvent = window.event;
  if (currentEvent === undefined) {
    // 默认的事件优先级 32 没有任何事件就是默认的
    return DefaultEventPriority;
  }
  // 不同的事件对应不同的优先级
  return getEventPriority(currentEvent.type);
}

export function shouldSetTextContent(type, props) {
  return (
    type === "textarea" ||
    type === "noscript" ||
    typeof props.children === "string" ||
    typeof props.children === "number" ||
    (typeof props.dangerouslySetInnerHTML === "object" &&
      props.dangerouslySetInnerHTML !== null &&
      props.dangerouslySetInnerHTML.__html != null)
  );
}

export function createInstance(
  type,
  props,
  rootContainerInstance,
  hostContext,
  internalInstanceHandle
) {
  // 创建原属
  const domElement = createElement(type, props);
  // 提前缓存fiber节点到真实dom上
  //  node["__reactFiber$" + randomKey] = hostInst;
  precacheFiberNode(internalInstanceHandle, domElement);
  // 更新的是fiber的属性 不是dom属性
  // node["__reactProps$" + randomKey] = props
  updateFiberProps(domElement, props);
  return domElement;
}

export function appendInitialChild(parentInstance, child) {
  parentInstance.appendChild(child);
}

// 挂载新节点 将真实dom节点挂到自己身上
export function finalizeInitialChildren(domElement, type, props) {
  // 设置初始属性
  setInitialProperties(domElement, type, props);
  switch (type) {
    case "button":
    case "input":
    case "select":
    case "textarea":
      return !!props.autoFocus;
    case "img":
      return true;
    default:
      return false;
  }
}

export function prepareUpdate(
  domElement,
  type,
  oldProps,
  newProps,
  hostContext
) {
  // 比较属性的差异
  return diffProperties(domElement, type, oldProps, newProps);
}

// 删除
export function removeChildFromContainer(container, child) {
  if (container.nodeType === COMMENT_NODE) {
    container.parentNode.removeChild(child);
  } else {
    container.removeChild(child);
  }
}

export function removeChild(parentInstance, child) {
  parentInstance.removeChild(child);
}

export function commitTextUpdate(textInstance, oldText, newText) {
  textInstance.nodeValue = newText;
}

// 提交 diffProperties 产生的更新 差异数组
export function commitUpdate(
  domElement,
  updatePayload,
  type,
  oldProps,
  newProps,
  internalInstanceHandle
) {
  // 更新dom属性
  updateProperties(domElement, updatePayload, type, oldProps, newProps);
  // 更新fiber的属性
  updateFiberProps(domElement, newProps);
}

export function commitMount() {
  switch (type) {
  }
}
