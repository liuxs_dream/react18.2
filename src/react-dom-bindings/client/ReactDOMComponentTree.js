const randomKey = Math.random().toString(36).slice(2);

const internalContainerInstanceKey = "__reactContainer$" + randomKey;
const internalPropsKey = "__reactProps$" + randomKey;
const internalInstanceKey = "__reactFiber$" + randomKey;

export function markContainerAsRoot(hostRoot, node) {
  node[internalContainerInstanceKey] = hostRoot;
}

export function unmarkContainerAsRoot(node) {
  node[internalContainerInstanceKey] = null;
}

// 更新fiber的属性
export function updateFiberProps(node, props) {
  node[internalPropsKey] = props;
}

// 从真实的dom节点上获取对应的fiber节点
export function getClosestInstanceFromNode(targetNode) {
  let targetInst = targetNode[internalInstanceKey];
  if (targetInst) return targetInst;
  let parentNode = targetNode.parentNode;
  while (parentNode) {
    targetInst =
      parentNode[internalContainerInstanceKey] ||
      parentNode[internalInstanceKey];
    if (targetInst) {
      // const alternate = targetInst.alternate;
      // if (
      //   targetInst.child !== null ||
      //   (alternate !== null && alternate.child !== null)
      // ) {
      // }
      return targetInst;
    }
    targetNode = parentNode;
    parentNode = targetNode.parentNode;
  }
  return null;
}

// 在createInstance的时候会调用这个函数
export function precacheFiberNode(hostInst, node) {
  node[internalInstanceKey] = hostInst;
}

export function getFiberCurrentPropsFromNode(node) {
  return node[internalPropsKey] || null;
}
