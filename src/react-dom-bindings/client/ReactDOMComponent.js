import { setValueForStyles } from "./CSSPropertyOperations";
import setTextContent from "./setTextContent";
import { setValueForProperty } from "./DOMPropertyOperations";

const CHILDREN = "children";
const STYLE = "style";

export function createTextNode(text, rootContainerElement) {
  // return getOwnerDocumentFromRootContainer(rootContainerElement).createTextNode(
  //   text
  // );
  return document.createTextNode(text);
}

export function createElement(type, props) {
  const element = document.createElement(type);
  return element;
}

// 设置初始属性
export function setInitialProperties(domElement, tag, rawProps) {
  // const isCustomComponentTag = isCustomComponent(tag, rawProps);
  let props;
  switch (tag) {
    default:
      props = rawProps;
  }
  setInitialDOMProperties(tag, domElement, props);
  switch (tag) {
    default:
      break;
  }
}

function setInitialDOMProperties(tag, domElement, nextProps) {
  for (const propKey in nextProps) {
    if (!nextProps.hasOwnProperty(propKey)) {
      continue;
    }
    const nextProp = nextProps[propKey];
    if (propKey === STYLE) {
      // style[styleName] = styleValue;
      setValueForStyles(domElement, nextProp);
    } else if (propKey === CHILDREN) {
      // children 不需要单独处理 但是 string 和 number 是需要处理的
      if (typeof nextProp === "string") {
        // node.textContent = text
        setTextContent(domElement, nextProp);
      } else if (typeof nextProp === "number") {
        setTextContent(domElement, "" + nextProp);
      }
    } else if (nextProp != null) {
      // node.setAttribute(name, value);
      setValueForProperty(domElement, propKey, nextProp);
    }
  }
}

/**
 *  比较属性差异
 * @param {*} domElement
 * @param {*} tag
 * @param {*} lastRawProps
 * @param {*} nextRawProps
 * @returns updatePayload 差异的数组
 */
export function diffProperties(domElement, tag, lastRawProps, nextRawProps) {
  let updatePayload = null;
  let lastProps;
  let nextProps;
  switch (tag) {
    default:
      lastProps = lastRawProps;
      nextProps = nextRawProps;
      break;
  }

  let propKey;
  let styleName;
  let styleUpdates = null;

  // 老属性中有 新的里面没有就需要删除
  for (propKey in lastProps) {
    if (
      nextProps.hasOwnProperty(propKey) ||
      !lastProps.hasOwnProperty(propKey) ||
      lastProps[propKey] === null
    ) {
      continue;
    }
    // 样式
    if (propKey === STYLE) {
      const lastStyle = lastProps[propKey];
      for (styleName in lastStyle) {
        if (lastStyle.hasOwnProperty(styleName)) {
          if (!styleUpdates) {
            styleUpdates = {};
          }
          styleUpdates[styleName] = "";
        }
      }
    } else {
      // 要删除属性
      (updatePayload = updatePayload || []).push(propKey, null);
    }
  }

  for (propKey in nextProps) {
    const nextProp = nextProps[propKey]; // 新的属性
    const lastProp = lastProps != null ? lastProps[propKey] : undefined; // 老的属性
    if (
      !nextProps.hasOwnProperty(propKey) ||
      nextProp === lastProp ||
      (nextProp == null && lastProp == null)
    ) {
      continue;
    }
    if (propKey === STYLE) {
      if (lastProp) {
        for (styleName in lastProp) {
          if (
            lastProp.hasOwnProperty(styleName) &&
            (!nextProp || !nextProp.hasOwnProperty(styleName))
          ) {
            if (!styleUpdates) {
              styleUpdates = {};
            }
            styleUpdates[styleName] = "";
          }
        }
        for (styleName in nextProp) {
          if (
            nextProp.hasOwnProperty(styleName) &&
            lastProp[styleName] !== nextProp[styleName]
          ) {
            if (!styleUpdates) {
              styleUpdates = {};
            }
            styleUpdates[styleName] = nextProp[styleName];
          }
        }
      } else {
        if (!styleUpdates) {
          if (!updatePayload) {
            updatePayload = [];
          }
          updatePayload.push(propKey, styleUpdates);
        }
        styleUpdates = nextProp;
      }
    } else if (propKey === CHILDREN) {
      // children是单独处理的 但是文本的是特殊的
      if (typeof nextProp === "string" || typeof nextProp === "number") {
        (updatePayload = updatePayload || []).push(propKey, "" + nextProp);
      }
    } else {
      (updatePayload = updatePayload || []).push(propKey, nextProp);
    }
  }

  if (styleUpdates) {
    (updatePayload = updatePayload || []).push(STYLE, styleUpdates);
  }

  return updatePayload;
}

// 更新属性差异
export function updateProperties(
  domElement,
  updatePayload,
  tag,
  lastRawProps,
  nextRawProps
) {
  updateDOMProperties(
    domElement,
    updatePayload
    // wasCustomComponentTag,
    // isCustomComponentTag
  );
}

// 更新dom属性
function updateDOMProperties(domElement, updatePayload) {
  for (let i = 0; i < updatePayload.length; i += 2) {
    const propKey = updatePayload[i];
    const propValue = updatePayload[i + 1];
    if (propKey === STYLE) {
      // 样式
      setValueForStyles(domElement, propValue);
    } else if (propKey === CHILDREN) {
      // 单个文本的children
      setTextContent(domElement, propValue);
    } else {
      // 属性设置 setAttribute
      setValueForProperty(domElement, propKey, propValue);
    }
  }
}
