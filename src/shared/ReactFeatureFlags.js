export const deferRenderPhaseUpdateToNextBatch = false;
export const enableCapturePhaseSelectiveHydrationWithoutDiscreteEventReplay = true;
export const enableCreateEventHandleAPI = false;
export const allowConcurrentByDefault = true;
export const disableSchedulerTimeoutInWorkLoop = false;
export const enableSyncDefaultUpdates = true;
export const enableServerContext = true;
