export const REACT_ELEMENT_TYPE = Symbol.for("react.element");
export const REACT_PROVIDER_TYPE = Symbol.for("react.provider");
export const REACT_CONTEXT_TYPE = Symbol.for("react.context");
export const REACT_SERVER_CONTEXT_DEFAULT_VALUE_NOT_LOADED = Symbol.for(
  "react.default_value"
);
