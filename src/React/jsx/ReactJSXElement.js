import hasOwnProperty from "shared/hasOwnProperty";
import { REACT_ELEMENT_TYPE } from "../../shared/ReactSymbols";
export const ReactCurrentOwner = { current: null };

// 保留属性
const RESERVED_PROPS = {
  key: true,
  ref: true,
  __self: true,
  __source: true,
};

const ReactElement = function (type, key, ref, self, source, owner, props) {
  const element = {
    // 表示是react元素 react中大部分的 $$typeof就是这个
    $$typeof: REACT_ELEMENT_TYPE,
    type,
    key,
    ref,
    props,
    _owner: owner,
  };
  // Object.defineProperty(element, '_self', {})
  // Object.defineProperty(element, '_source', {})
  return element;
};

/**
 * 在react17之后新版的转化 key是在第三个参数的
 * children是在config中的
 * @param {*} type
 * @param {*} config
 * @param {*} maybeKey
 * @param {*} source
 * @param {*} self
 * @returns
 */
export function jsxDEV(type, config, maybeKey, source, self) {
  let propName; // 属性名
  const props = {}; // 属性对象
  let key = null; // 区分不同的子节点
  let ref = null; // 引用 {current: null} useRef createRef
  if (maybeKey !== undefined) {
    key = "" + maybeKey;
  }
  if (config.key !== undefined) {
    key = "" + config.key;
  }
  if (config.ref !== undefined) {
    ref = config.ref;
  }
  // 遍历属性赋值
  for (propName in config) {
    // 保留的属性是不拷贝的
    if (
      hasOwnProperty.call(config, propName) &&
      !RESERVED_PROPS.hasOwnProperty(propName)
    ) {
      props[propName] = config[propName];
    }
  }
  // 返回react元素
  return ReactElement(
    type,
    key,
    ref,
    self,
    source,
    ReactCurrentOwner.current,
    props
  );
}
