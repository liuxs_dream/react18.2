import ReactCurrentDispatcher from "./ReactCurrentDispatcher";

export const ReactSharedInternals = {
  // {current: null}
  ReactCurrentDispatcher,
};

export default ReactSharedInternals;
