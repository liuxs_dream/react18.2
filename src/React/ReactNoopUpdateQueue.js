const ReactNoopUpdateQueue = {
  isMounted() {
    return false;
  },
  enqueueSetState() {
    console.error("noop");
  },
};

export default ReactNoopUpdateQueue;
