import { createElement } from "./ReactElement";
import { Component, PureComponent } from "./ReactBaseClasses";
import { ReactSharedInternals as __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED } from "./ReactSharedInternals";
import { createContext } from "./ReactContext";

import {
  // useCallback,
  // useContext,
  useEffect,
  // useEffectEvent,
  useLayoutEffect,
  // useMemo,
  useReducer,
  useRef,
  useState,
  // useTransition,
  // useDeferredValue,
  // useId,
  // use,
  useContext,
} from "./ReactHooks";

const React = {
  createElement,
  Component,
  PureComponent,
  __SECRET_INTERNALS_DO_NOT_USE_OR_YOU_WILL_BE_FIRED,
  useReducer,
  useState,
  useEffect,
  useLayoutEffect,
  useRef,
  useContext,
  createContext,
};

export default React;
